
Copyright (c) 2021 - CacheIT-Team - All Rights Reserved
Unauthorized copying of these files, via any medium is strictly prohibited, with the exception of the BaseDao implementation.
Proprietary and confidential, with the exception of the BaseDao implementation.

Written by:
 - Marcel van der Heide
 - Tim Vogel
 - Frieder Ullmann
