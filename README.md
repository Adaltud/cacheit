[![pipline status](https://gitl4b.dutches.de/fhe/pme/cacheit/badges/master/pipeline.svg)](https://gitl4b.dutches.de/fhe/pme/cacheit/commits/master) <br><br>

[![forthebadge](https://forthebadge.com/images/badges/fuck-it-ship-it.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/as-seen-on-tv.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/winter-is-coming.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/designed-in-ms-paint.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/contains-cat-gifs.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/uses-badges.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/compatibility-betamax.svg)](https://forthebadge.com)

<br>

![CacheIt Logo](images/Logo__Halbe_Größe_.png)


# CacheIT

Die Installationsanleitung finden sie [hier](#installation).

## Beschreibung

CacheIT die App für Digitales Geo-Caching!<br>
### Beginn
#### Anmeldung / Registration
Nach der Installation wird ein Profil benötigt. Im SignUp Fenster können sie sich einen Account erstellen bzw. den erstellten dann später über SignIn verwenden.
#### Übersicht
Der erste Reiter der Bottomnavigation beeinhaltet ein Dashboard, welches zur Zeit jedoch noch keinen nenneswerten Inhalt aufweist.

Im zweiten Reiter hat man eine Übersicht über alle erstellten Caches, RouteParts und Routen.
Jede Route besteht aus mindestens einem RoutePart und jeder RoutePart aus genau 2 Caches. Einem Start- und einem Finishcache.
Über das Menü im zweiten Reiter kann man außerdem weitere Caches anlegen, diese zu RouteParts kombinieren und aus diesen neue Routen bauen.

Im dritten Reiter, findet man eine große Karte, auf der alle Routen in der Umgebung angezeigt werde. Hier sind die Funktionalitäten jedoch noch sehr begrenzt.

Im vierten Reiter, bekommt man eine Übersicht über das eigene Profil, mit der Möglichkeit die eigenen Daten auch zu bearbeiten.

#### Route aussuchen
Wählt man nun eine Route in dem zweiten Reiter aus, bekommt man die Möglichkeit die Route zu spielen und die Schatzsuche beginnt.

#### Route starten 
Man bekommt automatisch den ersten Cache, des ersten Routenteils, der Route angezeigt und muss nun mit Hilfe einer NFC oder 
QR Eingabe (über die FloatingactionBtns) verifizieren, dass man den Cache gefunden hat, welcher an eben jenen angezeigten Koordinaten zu finden ist. 
Zu Testzwecken gibt es hier noch einen Skipknopf, um die Aktion des Findens zu simulieren. 
Nun wird der nächste zu findende Cache angezeigt. Wurde der letzte Cache gescanned/geskipt springt die Ansicht 
automatisch in die Grundansicht des zweiten Reiters zurück. 

Zukünftig wird an dieser Stelle die Möglichkeit für ein Feedback gegeben, die geschaffte Route wird abgespeichert und in dem Profil angezeigt.
Angedacht ist in diesem Zuge auch ein Ratingsystem über das die Routen bewertet werden können.

#### Routen selber erstellen
Jeder Nutzer der App, wird in Zukunft in der Lage sein, selbst Caches in der realen Welt zu plazieren. 
Diese Caches können NFC/QR Tags an bereits vorhandenen traditionellen Caches sein oder auch neu plazierte werden. 
Durch die wesentlich geringere Größe und im Falle der QR-Codes auch umweltverträglichere Veranlagung, wird hier die Umwelt weniger beeinflusst
und eine Nutzung für z.B. Stadttouren oder Wanderpfade ist damit auch denkbar.

Diese angelegten Caches können dann, von jedem Nutzer frei zu neuen Routenteilen rekombiniert werden. 
Zukünftig können an diese Routeteile noch Aufgaben geknüpft werden, welche gelöst werden müssen bevor man die nächsten Koordinaten bekommt.

Diese Routenteile können nun wiederum von jedem zu neuen Routen rekombiniert werden. 
Dadurch kann sich eine eigene Dynamik entwickeln und jeder kann seiner Kreativität freien Lauf lassen, 
ohne zwangsweise selbst Caches platzieren zu müssen.

#### Offline / Online
Im aktuellen Entwicklungsstand funktioniert die App nur wenn sie eine Verbindung mit dem zugehörigen Server aufbauen kann.
Zukünftig werden jedoch die Daten nur synchronisiet, wenn eine Verbidung besteht und ansonsten kann die App im offline Betrieb voll verwendet werden
Getätigte Änderungen, Kommentare etc. werden zum nächst möglichen Zeitpunkt mit der zentralen Datenbank synchronisiert.


## Zentrale Features

- Routen spielen und selbst erstellen

- Mithilfe von Caches Routenteile und damit Routen erstellen

- ZUKÜNFITG: Jede Route kann mit Hilfe eines  Ratings bewerten

- In der App kommen neben den GPS Funktionen, auch die Kamera und die NFC Funktion zum Zug, um die Schnitzeljagd Digitaler zu gestalten



## Zukünftige Features

- Feedback und Rating Funktionen

- Aufgaben in den Routenteilen


## Technisches

Bestimme Knöpfe, für Funktionen welche Kamera, GPS oder NFC benötigen, werden automatisch ausgeblendet, 
falls das Anwendergerrät diese Funktionen nicht unterstützt. Für den Betrieb ist jedoch mindestens eine Kamera oder ein NFC Modul notwendig.

Der Server kann zur Zeit Daten zur Verfügung stellen, lokal angelegte Daten werden jedoch noch nicht auf den Server synchronisiert.

### Datenbankrelationen

Die App verwendet eine lokale Datenbank und wird mithilfe von [Room](https://developer.android.com/jetpack/androidx/releases/room) verwaltet. 

Da Room allerdings keine einfache, unkomplizierte möglichkeit bietet um mit Relationen umzugehen, wurde schlicht eine eigene Umsetzung geschrieben.

Um eine Relation in CacheIT zu definieren, müssen lediglich folgende Schritte druchgeführt werden:
1. Relationsfeld mit der Annotation `de.cacheit.model.relations.Relation` kennzeichnen, und Relationstabellennamen sowie DAO des verknüpften Typs angeben.
2. Eine Relationsentität definieren. Hierfür muss lediglich ein bestehender Relationsentität kopiert werden und anschließend eine neuen Tabellennamen sowie die entsprechenden Entitäten angegeben werden.
3. ???
4. Profit

Mithilfe der entsprechene DAO-Methoden, welche `ComplexAuto` im Namen enthalten, kann im Anschluss das entsprechende Objekt sowohl abgerufen als auch gespeichert werden. (inklusive verknüpfter Felder/Relationen)

Die Relationen werden im Hintergrund automatisch mit Hilfe der `BaseDao`-Klasse aufgelöst. 
Die dabei gefundenen Datensätze werden den jeweiligen Objekten zugeordnet


## UML Diagramm des Models

![UML mit Logo](UML_28.03.png)


### Verwendete Tools

  

**Drittbibliotheken**:


 | Libary	| License |
| ------------------ | ------------------ |
|  Dagger Hilt  | [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt)		        |
|  Lombok 	    | [MIT license](http://www.opensource.org/licenses/mit-license.php)	        | 
|  MariaDb 	    | [GPL license](https://mariadb.com/kb/en/mariadb-license/)		            | 
|  Moshi 	    | [Apache 2.0](https://github.com/square/moshi/blob/master/LICENSE.txt)     |
|  Retrofit 	| [Apache 2.0](https://github.com/square/retrofit/blob/master/LICENSE.txt)  |
|  Room 	    | [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt)		        |


**Versionskontrollsystem**

- GitLab



**Kommunikation**

- Discord

- GitLab

- Slack

- WhatsApp




# Installation

## Herrunterladen von CacheIT App und CacheITServer

Main Repository herunterladen: [Hier Klicken](https://gitl4b.dutches.de/fhe/pme/cacheit.git)
bzw. das Projekt clonen.
Zusätlich muss der CacheITServer gestartet werden, diesen bekommt man in unserem GitLab unter: [Hier Klicken](https://gitl4b.dutches.de/fhe/pme/cacheit-server.git)

## Starten des CacheITServer

Zuerst muss mit Maven Compiliert werden.
Im Anschluss kann der CacheITServer gestartet werden.

## Vorbereitungen für die App

Da der Server momentan auf dem eigenen System gehostet wir muss die eigene IP in der App hinterlegt werden, 
damit diese den Server lokalisieren kann.

Im Verzeichnis: \cacheit\app\src\main\java\de\cacheit\storage\remote\repositories
befindet sich die Datei RBaseRepository hier muss die BASE_URL, auf die eigenen IP Adresse angepasst werden + :8080/api/v1/.

Zusätlich muss die IP Adresse in der Datei network_security_config.xml hinzugefügt werden.
Die Datei ist zu finden under: \cacheit\app\src\main\res 
Die IP ist nach diesem Beispiel anzulegen: \<domain includeSubdomains="true">[IP-Adresse]\</domain>

## Start der CacheIT App

Bevor die App gestartet werden kann muss Gradle Compiliert werden, nach dem erfolgreichen Erstellen kann die App gestartet werden.

Hinweis: um alle Funktionen zu nutzen benötigt das Handy NFC, die Kamera und GPS.

### Entwickler

Frieder Ullmann, Tim Vogel, Marcel van der Heide
