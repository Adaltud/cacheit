package de.cacheit.utilities

import de.cacheit.model.Person
import java.time.LocalDate
import java.util.*
import java.util.concurrent.ThreadLocalRandom


object InstanceHelper {

    fun createPersonMale(): Person {
        val p = Person(rndFirstname(false), rndEmail(), rndString())
        p.id = rndLong(1, Long.MAX_VALUE)
        return p
    }

    fun createPersonFemale(): Person {
        val p = Person(rndFirstname(true), rndEmail(), rndString())
        p.id = rndLong(1, Long.MAX_VALUE)
        return p
    }

    fun createPerson(): Person {
        return if (rndInt(1, 2) == 1) createPersonFemale() else createPersonMale()
    }


    /* ######### HELPERS ####### */
    private fun rndString(): String {
        return UUID.randomUUID().toString().replace("-", "")
    }

    private fun rndInt(min: Int, max: Int): Int {
        return ThreadLocalRandom.current().nextInt(min, max)
    }

    private fun rndLong(min: Long, max: Long): Long {
        return ThreadLocalRandom.current().nextLong(min, max)
    }

    private fun rndDouble(min: Double, max: Double): Double {
        return ThreadLocalRandom.current().nextDouble(min, max)
    }

    private fun rndEmail(): String {
        return rndString() + "@example.com"
    }

    private fun rndCountry(): String {
        val name = arrayOf(
            "Germany",
            "England",
            "USA",
            "France",
            "Netherlands",
            "Belgium",
            "Denmark",
            "Swiss",
            "Canada"
        )
        return name[Random().nextInt(name.size)]
    }

    private fun rndCurrency(): Currency {
        val name = arrayOf("EUR", "CHF", "CNY", "PLN", "CZK", "RUB", "USD")
        return Currency.getInstance(name[Random().nextInt(name.size)])
    }

    private fun rndFirstname(female: Boolean): String {
        val maleName = arrayOf(
            "Jan",
            "Lukas",
            "Frieder",
            "Tim",
            "Tom",
            "Peter",
            "Niclas",
            "Danny",
            "Kevin",
            "Noel",
            "Hans",
            "Marcel",
            "Jonas"
        )
        val femaleName =
            arrayOf("Franziska", "Jenny", "Petra", "Ines", "Manuela", "Olga", "Kim", "Nadia")
        return if (female) femaleName[Random().nextInt(femaleName.size)] else maleName[Random().nextInt(
            maleName.size
        )]
    }

    private fun rndLastname(): String {
        val name = arrayOf(
            "Vogel",
            "Müller",
            "Schmidt",
            "Ullmann",
            "Mustermann",
            "Hecht",
            "Schröder",
            "Fischer",
            "Schneider"
        )
        return name[Random().nextInt(name.size)]
    }

    private fun rndLocalDate(minyear: Int, maxyear: Int): LocalDate {
        return LocalDate.of(rndInt(minyear, maxyear), rndInt(1, 12), rndInt(1, 28))
    }

    private fun rndLocalDate(minyear: Int, maxyear: Int, minmonth: Int, maxmonth: Int): LocalDate {
        return LocalDate.of(rndInt(minyear, maxyear), rndInt(minmonth, maxmonth), rndInt(1, 28))
    }

    private fun rndPlaceTitle(): String {
        val title = arrayOf(
            "Erfurter Dom",
            "Kölner Dom",
            "Brandenburger Tor",
            "Berlin Mitte",
            "FH-Erfurt",
            "Uni Erfurt",
            "Veste Coburg",
            "Nürnberger Flughafen",
            "Erfurter HBF",
            "Münchner HBF",
            "Berlin HBF",
            "Berlin Westbahnhof"
        )
        return title[Random().nextInt(title.size)]
    }
}
