package de.cacheit.core.extensions

import androidx.lifecycle.LiveData

/**
 * Returns null if the LivaData value object is null, otherwise the original LiveData.
 *
 * @return Original LiveData or null
 */
fun <T> LiveData<T>.emptyToNull(): LiveData<T>? {
    return if (this.value == null) null else this
}


/**
 * Returns null if the LivaData value list is null, otherwise the original LiveData.
 *
 * @return Original LiveData or null
 */
@JvmName("emptyListToNull")
fun <T> LiveData<List<T>>.emptyToNull(): LiveData<List<T>>? {
    return if (this.value == null || this.value.isNullOrEmpty()) null else this
}