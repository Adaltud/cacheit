package de.cacheit.core.extensions

/**
 * Returns null if the list is empty, otherwise the original list.
 *
 * @return Original list or null
 */
fun <T> List<T>.emptyToNull(): List<T>? {
    return if (this.isNullOrEmpty()) null else this
}
