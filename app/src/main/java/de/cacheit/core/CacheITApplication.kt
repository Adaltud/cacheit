package de.cacheit.core

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import de.cacheit.model.Person
import de.cacheit.model.route.Route
import de.cacheit.storage.local.dao.PersonDao
import de.cacheit.storage.repositories.BaseRepository
import de.cacheit.storage.repositories.PersonRepository
import kotlinx.coroutines.runBlocking
import java.lang.Exception
import javax.inject.Inject


/*
    Central (Singleton) class to hold references to data/storage respectively manage access to those.
 */
@HiltAndroidApp
class CacheITApplication() : Application() {

    @Inject
    lateinit var repo: PersonRepository

    override fun onCreate() {
        super.onCreate()
        instance = this
       //runBlocking { simpleDbTest() }
    }

    private suspend fun simpleDbTest(){
        // region variables
        var p1 = Person("PARENT", "mnb", "asd")
        var p2 = Person("CHILD", "mnb", "asd")
        var r1 = Route("Route 1", "No Desc", 0,0,0, null)
        var id2 = 0L
        p2.id = id2;
        p1.friends = mutableListOf()
        p1.friends?.add(p2)
        var id1 = 0L
        var p6: Person?
        var person: Person? = null
        // endregion

        // Insert person Test
        try {
            var id2 =  repo.insertAwait(p2)
        }
        catch (ex: Exception) {
            var x  = ex.localizedMessage
            return;
        }

        // Insert complex person test
        try {
            id1 = repo.insertComplexAutoAwait(p1)
        }
        catch (ex: Exception) {
            var x  = ex.localizedMessage
            return
        }


        // Get persons test
        try {
            p6 = repo.getByIdAwait(p1.id)
            person = repo.getByIdComplexAutoAwait(p1.id)!!
            person = repo.getByIdComplexAutoAwait(p1.id, "friends")!!
        }
        catch (ex: Exception) {
            var x  = ex.localizedMessage
            return
        }

        // try to access person
        var s = person.eMail
    }

    companion object {
        const val LOG_TAG = "AppClass"
        lateinit var instance: CacheITApplication private set
    }
}
