package de.cacheit.utils.osmdroid

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.icu.text.CaseMap
import androidx.core.content.ContextCompat
import de.cacheit.R
import de.cacheit.utils.Drawables
import de.cacheit.utils.Strings
import org.osmdroid.util.BoundingBox
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker

/**
 * Helper class to simplify the usage of the OSMDroid package.
 *
 */
class OSMDroidUtil {

    /**
     * Create a BoundingBox for multiple GeoPoints.
     * In other words, look for the points most north, south, east and west.
     *
     * @param listGeoPoints The list with GeoPoints for which the BoundingBox needs to be created.
     * @returnt The BoundingBox.
     */
    fun getBoundingBox(listGeoPoints: List<GeoPoint>) : BoundingBox {

        // Bouncer
        if (listGeoPoints.isEmpty())
            return BoundingBox(0.0,0.0,0.0,0.0)

        // Define a starting point
        var north = listGeoPoints[0].latitude
        var south = listGeoPoints[0].latitude
        var east = listGeoPoints[0].longitude
        var west = listGeoPoints[0].longitude

        listGeoPoints.forEach { point ->
            if (point.latitude > north)
                north = point.latitude

            if (point.latitude < south)
                south = point.latitude

            if (point.longitude > east)
                east = point.longitude

            if (point.longitude < west)
                west = point.longitude
        }

        return BoundingBox(north, east, south, west)
    }

    /**
     * Place a simple marker on an OSMDroid map.
     *
     * @param map The map to put the marker on.
     * @param point The coordinates where to place the marker.
     * @param title The title of the marker, shown when a user clicks on the marker.
     * @param icon Icon to display.
     */
    fun placeSimpleMarker(map: MapView, point: GeoPoint, title: String, icon: Drawable? = null) {
        var ICON = icon;

        val marker = Marker(map)
        marker.position = point
        marker.textLabelBackgroundColor = Color.TRANSPARENT
        marker.textLabelForegroundColor = Color.RED
        marker.textLabelFontSize = 40
        marker.title = title
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)

        if (ICON == null)
            ICON = Drawables.get(R.drawable.ic_map_marker_medium)

        marker.icon = ICON
        map.overlayManager?.add(marker);
    }
}