package de.cacheit.utils.nfc

import android.nfc.NdefRecord
import android.nfc.Tag
import android.nfc.tech.Ndef
import android.util.Log
import java.lang.Exception
import kotlin.experimental.and

/**
 * Helper class to simplify the usage of the NFC functionality
 *
 */
class NFCUtil {

    /**
     * Get the message on a NFC-Tag, contained in the records.
     *
     * @param tag The tag from where to retrieve the message.
     * @return The joined message, converted to string.
     */
    fun getJoinedMessage(tag: Tag): String {
        val ndef = Ndef.get(tag)
        val ndefMsg = ndef?.cachedNdefMessage
        val ndefRecords = ndefMsg?.records

        return if (ndefRecords != null)
            getJoinedMessage(ndefRecords)
        else "";
    }

    /**
     * Get the message on a NFC-Tag, contained in the records.
     *
     * @param records The records from where to retrieve the message.
     * @return The joined message, converted to string.
     */
    fun getJoinedMessage(records: Array<NdefRecord>): String {
        var msg: String = ""
        records.forEach { x -> msg += getMessage(x) + System.lineSeparator() }
        return msg
    }

    /**
     * Get the message on a NFC-Tag, contained in a specific record.
     *
     * @param record The record from where to retrieve the message.
     * @return The message, converted to string.
     */
    fun getMessage(record: NdefRecord): String {
        val payload: ByteArray = record.payload

        // figure out if we need to take out the " en" at the beginning
        val textEncoding = if(payload[0] and 128.toByte() == 0.toByte()) "UTF-8" else "UTF-16"
        val langCodeLength = payload[0] and 63.toByte()

        return String(
            payload, langCodeLength + 1, payload.count() - langCodeLength - 1, charset(
                textEncoding
            )
        )
    }

    /**
     * Get the serial number (id) from an NFC-Tag
     *
     * @param tag The tag from where to retrieve the serial number
     * @return The serial number, converted to string
     */
    fun getIdString(tag: Tag): String {
        return try {
            byteArrayToHexString(tag.id);
        } catch (ex: Exception) {
            Log.e("NFC", "Unable to read NFC Id", ex)
            "";
        }
    }

    private fun byteArrayToHexString(inarray: ByteArray): String {
        var i: Int
        var `in`: Int
        val hex =
            arrayOf("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F")
        var out = ""
        var j: Int = 0
        while (j < inarray.size) {
            `in` = inarray[j].toInt() and 0xff
            i = `in` shr 4 and 0x0f
            out += hex[i]
            i = `in` and 0x0f
            out += hex[i]
            ++j
        }
        return out
    }
}