package de.cacheit.utils

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.StringRes
import de.cacheit.core.CacheITApplication

/**
 * Helper method to get a resource string, without context
 */
object Strings {
    fun get(@StringRes stringRes: Int, vararg formatArgs: Any = emptyArray()): String {
        return CacheITApplication.instance.getString(stringRes, *formatArgs)
    }
}

/**
 * Helper method to get a drawable, without context
 */
object Drawables {
    fun get(stringRes: Int): Drawable? {
        return CacheITApplication.instance.getDrawable(stringRes)
    }
}

/**
 * Helper method to get a system service, without context
 */
object SystemServices {
    fun get(service: String): Any {
        return CacheITApplication.instance.getSystemService(service)
    }
}

/**
 * Helper method to get the current application context, without context
 */
object Contexts {
    fun get() : Context {
        return CacheITApplication.instance.applicationContext
    }
}