package de.cacheit.model.route

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore

import de.cacheit.model.BaseModel
import de.cacheit.model.IEntityCreatedBy
import de.cacheit.model.Person
import de.cacheit.model.relations.RelPersonSolvedRoutes
import de.cacheit.model.relations.Relation
import de.cacheit.model.relations.route.RelRouteCreatedBy
import de.cacheit.model.relations.route.RelRouteRouteParts
import de.cacheit.model.relations.route.RelRouteStartRoutePart
import de.cacheit.storage.local.dao.PersonDao
import de.cacheit.storage.local.dao.route.RouteDao
import de.cacheit.storage.local.dao.route.RoutePartDao
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Entity(tableName = Route.TABLE_NAME)
@Parcelize
data class Route (

    /**
     * Name of the Route.
     */
    @ColumnInfo(name = "name")
    var name: String = "",

    /**
     * Description of the Route.
     */
    @ColumnInfo(name = "description")
    var description: String = "",

    /**
     * Current rating of the Route.
     */
    @ColumnInfo(name = "rating")
    var rating: Int = 0,

    /**
     * Number of successfully solved playthroughs.
     */
    @ColumnInfo(name = "solved_counter")
    var solvedCounter: Int = 0,

    /**
     * Number of views on this Route.
     */
    @ColumnInfo(name = "view_counter")
    var viewCounter: Int = 0,

    /**
     * Start RoutePart for this Route.
     */
    @Ignore
    @IgnoredOnParcel
    @Relation(RelRouteStartRoutePart.TABLE_NAME, RoutePartDao::class)
    var startRoutePart: RoutePart? = null,

    /**
     * All RouteParts for this Route.
     */
    @Ignore
    @IgnoredOnParcel
    @Relation(RelRouteRouteParts.TABLE_NAME, RoutePartDao::class)
    var routeParts: MutableList<RoutePart>? = null,

    /**
     * User who created this record.
     */
    @Ignore
    @Relation(RelRouteCreatedBy.TABLE_NAME, PersonDao::class)
    override var createdBy: Person? = null

) : BaseModel(), Parcelable, IEntityCreatedBy {
    companion object {

        /**
         * Entity table name.
         */
        const val TABLE_NAME = "route"
    }
}