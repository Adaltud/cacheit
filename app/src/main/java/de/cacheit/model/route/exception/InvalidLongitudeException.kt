package de.cacheit.model.route.exception

/**
 * Should be thrown if a longitude value is not in the bounds of -180 and 180
 */
class InvalidLongitudeException(message: String?) : Exception(message)
