package de.cacheit.model.route

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.util.TableInfo.Column
import de.cacheit.model.BaseModel
import de.cacheit.model.Person
import kotlinx.android.parcel.Parcelize

@Entity(tableName = Coordinates.TABLE_NAME)
@Parcelize
data class Coordinates (

    /**
     * Latitude value of the coordinate.
     */
    @ColumnInfo(name = "latitude")
    var latitude: Double = 0.0,

    /**
     * Longitude value of the coordinate.
     */
    @ColumnInfo(name = "longitude")
    var longitude: Double = 0.0

) : BaseModel(), Parcelable {
    companion object {

        /**
         * Entity table name.
         */
        const val TABLE_NAME = "coordinates"
    }
}