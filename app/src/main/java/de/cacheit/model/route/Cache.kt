package de.cacheit.model.route

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.util.TableInfo.Column

import de.cacheit.model.BaseModel
import de.cacheit.model.IEntityCreatedBy
import de.cacheit.model.Person
import kotlinx.android.parcel.Parcelize
import de.cacheit.model.relations.Relation
import de.cacheit.model.relations.route.RelCacheCoordinates
import de.cacheit.model.relations.route.RelCacheCreatedBy
import de.cacheit.model.relations.route.RelRouteStartRoutePart
import de.cacheit.storage.local.dao.PersonDao
import de.cacheit.storage.local.dao.route.CoordinatesDao
import de.cacheit.storage.local.dao.route.RoutePartDao

@Entity(tableName = Cache.TABLE_NAME)
@Parcelize
data class Cache (

    /**
     * Name of the cache.
     */
    @ColumnInfo(name = "name")
    var name: String = "",

    /**
     * Description of the cache.
     */
    @ColumnInfo(name = "description")
    var description: String = "",

    /**
     * NFC-Code or QR-Code needed to get the next cache.
     */
    @ColumnInfo(name = "nfccode")
    var NFCCode: String = "",

    /**
     * Location of the cache
     */
    @Ignore
    @Relation(RelCacheCoordinates.TABLE_NAME, CoordinatesDao::class)
    var coordinates: Coordinates? = null,

    /**
     * User who created this record.
     */
    @Ignore
    @Relation(RelCacheCreatedBy.TABLE_NAME, PersonDao::class)
    override var createdBy: Person? = null

) : BaseModel(), Parcelable, IEntityCreatedBy {

    /**
     * Entity table name.
     */
    companion object {
        const val TABLE_NAME = "cache"
    }
}
