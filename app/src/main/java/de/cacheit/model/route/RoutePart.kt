package de.cacheit.model.route

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore

import de.cacheit.model.BaseModel
import de.cacheit.model.IEntityCreatedBy
import de.cacheit.model.Person
import de.cacheit.model.challenge.Challenge
import de.cacheit.model.relations.Relation
import de.cacheit.model.relations.route.RelRoutePartCacheFinish
import de.cacheit.model.relations.route.RelRoutePartCacheUnlock
import de.cacheit.model.relations.route.RelRoutePartChallenge
import de.cacheit.model.relations.route.RelRoutePartCreatedBy
import de.cacheit.storage.local.dao.PersonDao
import de.cacheit.storage.local.dao.challenge.TextChallengeDao
import de.cacheit.storage.local.dao.route.CacheDao
import kotlinx.android.parcel.Parcelize

@Entity(tableName = RoutePart.TABLE_NAME)
@Parcelize
data class RoutePart (

    /**
     * Name of this RoutePart.
     */
    @ColumnInfo(name = "name")
    var name: String = "",

    /**
     * Deprecated, will be deleted in the near future.
     */
    @ColumnInfo(name = "completed")
    var completed: Boolean = false,

    /**
     * Description of this RoutePart.
     */
    @ColumnInfo(name = "description")
    var description: String = "",

    /**
     * The challenge to solve to unlock the next cache.
     */
    @Ignore
    @Relation(RelRoutePartChallenge.TABLE_NAME, TextChallengeDao::class)
    var challenge: Challenge? = null,

    /**
     * The cache to unlock the next RoutePart.
     */
    @Ignore
    @Relation(RelRoutePartCacheUnlock.TABLE_NAME, CacheDao::class)
    var cacheToUnlock: Cache? = null,

    /**
     * The cache to finish this RoutePart.
     */
    @Ignore
    @Relation(RelRoutePartCacheFinish.TABLE_NAME, CacheDao::class)
    var cacheToFinishPart: Cache? = null,

    /**
     * User who created this record.
     */
    @Ignore
    @Relation(RelRoutePartCreatedBy.TABLE_NAME, PersonDao::class)
    override var createdBy: Person? = null

) : BaseModel(), Parcelable, IEntityCreatedBy {
    companion object {

        /**
         * Entity table name.
         */
        const val TABLE_NAME = "routepart"
    }
}