package de.cacheit.model

import android.content.Context
import androidx.room.*

/**
 * Base model for every entity.
 * Defines basic properties
 *
 * @property id Record identifier
 * @property created Creation time of record
 * @property modified Last modification or creation time of record
 * @property online Defines if the record is available in the remote data store
 * @property pending_changes Defines if record changes are pending and should be pushed to the remote data store
 */
abstract class BaseModel(
    /**
     * Record identifier
     * If "online" is false, this identifier is only locally valid, and will be overwritten on next
     * synchronisation with the remote data store
     */
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0, // 0 equals as not set => https://developer.android.com/reference/android/arch/persistence/room/PrimaryKey

    /**
     * Creation time of this record
     */
    @ColumnInfo(name = "created") // @NonNull not needed, since Kotlin implies that these values cannot be null without the question mark
    var created: Long = System.currentTimeMillis(),

    /**
     * Last modification or creation time of this record
     */
    @ColumnInfo(name = "modified")
    var modified: Long = System.currentTimeMillis(),

    /**
     * Defines whether this record is already online available (remote data store)
     */
    @ColumnInfo(name = "online")
    var online: Boolean = false,

    /**
     * Defines whether this record was updated recently, but the changes need to be pushed to the remote data store
     */
    @ColumnInfo(name = "pending_changes")
    var pending_changes: Boolean = false,

    /**
     * Defines whether this recors was deleted by the user, if so, than this record needs to be deleted
     * in the remote data store on next sync
     */
    @ColumnInfo(name = "deleted")
    var deleted: Boolean = false
)
