package de.cacheit.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import de.cacheit.model.relations.RelFeedbackCreatedBy
import de.cacheit.model.relations.RelFeedbackRoute
import de.cacheit.model.relations.Relation
import de.cacheit.model.route.Route
import de.cacheit.storage.local.dao.PersonDao
import de.cacheit.storage.local.dao.route.RouteDao
import kotlinx.android.parcel.Parcelize

@Entity(tableName = Feedback.TABLE_NAME)
@Parcelize
data class Feedback(

    /**
     * Deprecated, will be deleted in newer version.
     * The id of the user who created this feedback.
     */
    @ColumnInfo(name = "user_id")
    var userId: Long = 0,

    /**
     * Deprecated, will be deleted in newer version.
     * The id of the route for this feedback.
     */
    @ColumnInfo(name = "route_id")
    var routeId: Long = 0,

    /**
     * The content of the feedback.
     */
    @ColumnInfo(name = "feedback")
    var feedback: String = "",

    /**
     * The rating given in this feedback.
     */
    @ColumnInfo(name = "rating")
    var rating: Int = 0,

    /**
     * User who created this record.
     */
    @Ignore
    @Relation(RelFeedbackCreatedBy.TABLE_NAME, PersonDao::class)
    override var createdBy: Person? = null,

    /**
     * The route for this feedback.
     */
    @Ignore
    @Relation(RelFeedbackRoute.TABLE_NAME, RouteDao::class)
    var route: Route? = null,

) : BaseModel(), Parcelable, IEntityCreatedBy {
    companion object {

        /**
         * Entity table name.
         */
        const val TABLE_NAME = "feedback"
    }
}