package de.cacheit.model.challenge

import android.os.Parcelable
import androidx.room.ColumnInfo
import de.cacheit.model.BaseModel
import de.cacheit.model.IEntityCreatedBy


abstract class Challenge (

    /**
     * Quiz question to solve.
     */
    @ColumnInfo(name = "quizz")
    var quizz: String = "",

    /**
     * The correct answer for the question.
     */
    @ColumnInfo(name = "correct_answer")
    var correctAnswer: String = ""
) : BaseModel(), Parcelable, IEntityCreatedBy
