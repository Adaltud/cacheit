package de.cacheit.model.challenge

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Ignore
import de.cacheit.model.IEntityCreatedBy
import de.cacheit.model.Person
import de.cacheit.model.relations.Relation
import de.cacheit.model.relations.challenge.RelTextChallengeCreatedBy
import de.cacheit.storage.local.dao.PersonDao
import kotlinx.android.parcel.Parcelize

@Entity(tableName = TextChallenge.TABLE_NAME)
@Parcelize
data class TextChallenge (

    /**
     * Answer entered by user.
     */
    var answer: String

) : Challenge(), Parcelable, IEntityCreatedBy {

    /**
     * User who created this record.
     */
    @Ignore
    @Relation(RelTextChallengeCreatedBy.TABLE_NAME, PersonDao::class)
    override var createdBy: Person? = null

    companion object {

        /**
         * Entity table name.
         */
        const val TABLE_NAME = "challenge_text"
    }
}
