package de.cacheit.model

import android.os.Parcelable
import androidx.room.*
import de.cacheit.model.relations.RelPersonFriends
import de.cacheit.model.relations.RelPersonSavedRoutes
import de.cacheit.model.relations.RelPersonSolvedRoutes
import de.cacheit.model.relations.Relation
import de.cacheit.model.route.Route
import de.cacheit.storage.local.dao.PersonDao
import de.cacheit.storage.local.dao.route.RouteDao
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize


@Entity(tableName = Person.TABLE_NAME)
@Parcelize
data class Person(
    /**
     * The username of the user.
     */
    @ColumnInfo(name = "username")
    var userName: String = "",

    /**
     * Email of the user.
     */
    @ColumnInfo(name = "email")
    var eMail: String = "",

    /**
     * User password.
     * remarks: Currently not secured!
     */
    @ColumnInfo(name = "password")
    var password: String = ""

) : BaseModel(), Parcelable {

    /**
     * All routes solved by this user.
     */
    @Ignore
    @IgnoredOnParcel
    @Relation(RelPersonSolvedRoutes.TABLE_NAME, RouteDao::class)
    val solvedRoutes: MutableList<Route>? = null

    /**
     * All routes saved by this user.
     */
    @Ignore
    @IgnoredOnParcel
    @Relation(RelPersonSavedRoutes.TABLE_NAME, RouteDao::class)
    val savedRoutes: MutableList<Route>? = null

    /**
     * All friends of this user.
     */
    @Ignore
    @IgnoredOnParcel
    @Relation(RelPersonFriends.TABLE_NAME, PersonDao::class, true)
    var friends: MutableList <Person>? = null

    companion object {
        /**
         * Entity table name.
         */
        const val TABLE_NAME = "person"
    }
}