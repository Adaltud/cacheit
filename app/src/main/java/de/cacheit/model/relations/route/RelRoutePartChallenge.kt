package de.cacheit.model.relations.route

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.Index
import de.cacheit.model.Feedback
import de.cacheit.model.challenge.Challenge
import de.cacheit.model.challenge.TextChallenge
import de.cacheit.model.relations.RelBaseModel
import de.cacheit.model.route.Route
import de.cacheit.model.route.RoutePart

@Entity(tableName = RelRoutePartChallenge.TABLE_NAME,
    foreignKeys = [ ForeignKey(entity = RoutePart::class, parentColumns = ["id"], childColumns = ["parentId"]),
                    ForeignKey(entity = TextChallenge::class, parentColumns = ["id"], childColumns = ["childId"]) ], // TODO ! Challenge?
    indices = [ Index(value = ["parentId"], unique = false), Index(value = ["childId"], unique = false) ] )
data class RelRoutePartChallenge(@Ignore var routePartId: Long, @Ignore var challengeId: Long) : RelBaseModel(routePartId, challengeId)
{
    constructor() : this(0L,0L) // Dummy constructor for room, not nice but working
    companion object{ const val TABLE_NAME = "rel_routepart_challenge" }
}