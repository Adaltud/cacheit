package de.cacheit.model.relations

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.Index
import de.cacheit.model.Feedback
import de.cacheit.model.Person

@Entity(tableName = RelFeedbackCreatedBy.TABLE_NAME,
    foreignKeys = [ ForeignKey(entity = Feedback::class, parentColumns = ["id"], childColumns = ["parentId"]),
                    ForeignKey(entity = Person::class, parentColumns = ["id"], childColumns = ["childId"]) ],
    indices = [ Index(value = ["parentId"], unique = false), Index(value = ["childId"], unique = false) ] )
data class RelFeedbackCreatedBy(@Ignore var feedbackId: Long, @Ignore var personId: Long) : RelBaseModel(feedbackId, personId)
{
    constructor() : this(0L,0L) // Dummy constructor for room, not nice but working
    companion object{ const val TABLE_NAME = "rel_feedback_createdby" }
}