package de.cacheit.model.relations.route

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.Index
import de.cacheit.model.relations.RelBaseModel
import de.cacheit.model.route.Cache
import de.cacheit.model.route.Route
import de.cacheit.model.route.RoutePart

@Entity(tableName = RelRouteStartRoutePart.TABLE_NAME,
    foreignKeys = [ ForeignKey(entity = Route::class, parentColumns = ["id"], childColumns = ["parentId"]),
                    ForeignKey(entity = RoutePart::class, parentColumns = ["id"], childColumns = ["childId"]) ],
    indices = [ Index(value = ["parentId"], unique = false), Index(value = ["childId"], unique = false) ] )
data class RelRouteStartRoutePart(@Ignore var routeId: Long, @Ignore var routePartId: Long) : RelBaseModel(routeId, routePartId)
{
    constructor() : this(0L,0L) // Dummy constructor for room, not nice but working
    companion object{ const val TABLE_NAME = "rel_route_start_routepart" }
}