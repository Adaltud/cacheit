package de.cacheit.model.relations

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.Index
import de.cacheit.model.Person
import de.cacheit.model.route.Route
import java.lang.reflect.Constructor

@Entity(tableName = RelPersonSavedRoutes.TABLE_NAME,
    foreignKeys = [ ForeignKey(entity = Person::class, parentColumns = ["id"], childColumns = ["parentId"]),
                    ForeignKey(entity = Route::class, parentColumns = ["id"], childColumns = ["childId"]) ],
    indices = [ Index(value = ["parentId"], unique = false), Index(value = ["childId"], unique = false) ] )
data class RelPersonSavedRoutes(@Ignore var personId: Long, @Ignore var routeId: Long) : RelBaseModel(personId, routeId)
{
    constructor() : this(0L,0L) // Dummy constructor for room, not nice but working
    companion object{ const val TABLE_NAME = "rel_person_saved_routes" }
}