package de.cacheit.model.relations.challenge

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.Index
import de.cacheit.model.Person
import de.cacheit.model.challenge.Challenge
import de.cacheit.model.challenge.MultipleChoiceChallenge
import de.cacheit.model.relations.RelBaseModel

@Entity(tableName = RelMultipleChoiceChallengeCreatedBy.TABLE_NAME,
    foreignKeys = [ ForeignKey(entity = MultipleChoiceChallenge::class, parentColumns = ["id"], childColumns = ["parentId"]),
                    ForeignKey(entity = Person::class, parentColumns = ["id"], childColumns = ["childId"]) ],
    indices = [ Index(value = ["parentId"], unique = false), Index(value = ["childId"], unique = false) ] )
data class RelMultipleChoiceChallengeCreatedBy(@Ignore var challengeId: Long, @Ignore var personId: Long) : RelBaseModel(challengeId, personId)
{
    constructor() : this(0L,0L) // Dummy constructor for room, not nice but working
    companion object{ const val TABLE_NAME = "rel_multiplechoicechallenge_createdby" }
}