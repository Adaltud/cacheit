package de.cacheit.model.relations

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Ignore
import androidx.room.PrimaryKey

// Also look in the description at the RelPersonFriends class
/*
    Base model for relations. This is a simple workaround which allows us to create relations without
    the massive annotations required from room, and thus keeping the entities as simple as possible.
    It also allows us to manually load child objects on the fly.
 */
open class RelBaseModel(
        @ColumnInfo(name = RelBaseModel.PARENTFIELDNAME)
        @NonNull
        var parentId: Long,

        @ColumnInfo(name = RelBaseModel.CHILDFIELDNAME)
        @NonNull
        var childId: Long
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0L

    companion object{
        const val PARENTFIELDNAME = "parentId"
        const val CHILDFIELDNAME = "childId"
    }

}