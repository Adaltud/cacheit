package de.cacheit.model.relations.route

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.Index
import de.cacheit.model.relations.RelBaseModel
import de.cacheit.model.route.Cache
import de.cacheit.model.route.Coordinates
import de.cacheit.model.route.Route
import de.cacheit.model.route.RoutePart

@Entity(tableName = RelCacheCoordinates.TABLE_NAME,
    foreignKeys = [ ForeignKey(entity = Cache::class, parentColumns = ["id"], childColumns = ["parentId"]),
                    ForeignKey(entity = Coordinates::class, parentColumns = ["id"], childColumns = ["childId"]) ],
    indices = [ Index(value = ["parentId"], unique = false), Index(value = ["childId"], unique = false) ] )
data class RelCacheCoordinates(@Ignore var cacheId: Long, @Ignore var coordinatesId: Long) : RelBaseModel(cacheId, coordinatesId)
{
    constructor() : this(0L,0L) // Dummy constructor for room, not nice but working
    companion object{ const val TABLE_NAME = "rel_cache_coordinates" }
}