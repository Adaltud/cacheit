package de.cacheit.model.relations

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.Index
import de.cacheit.model.Person
import java.lang.reflect.Constructor

// And again, rooms limitations forces us to do useless stuff. We need to use a constructor, where at least 1 column is not ignored, sigh...
// Inheritance is a joke at this point
//
// Only:
// data class RelPersonFriends(@Ignore var personId: Long, @Ignore var friendId: Long) : RelBaseModel(personId, friendId)
//
// or...
//
// @Entity(tableName = RelPersonFriends.TABLE_NAME, ignoredColumns = ["personId", "friendId"])
// data class RelPersonFriends(var personId: Long, var friendId: Long) : RelBaseModel(personId, friendId)
//
// Are not allowed....

@Entity(tableName = RelPersonFriends.TABLE_NAME,
        foreignKeys = [ ForeignKey(entity = Person::class, parentColumns = ["id"], childColumns = ["parentId"]),
                        ForeignKey(entity = Person::class, parentColumns = ["id"], childColumns = ["childId"]) ],
        indices = [ Index(value = ["parentId"], unique = false), Index(value = ["childId"], unique = false) ] )
data class RelPersonFriends(@Ignore var personId: Long, @Ignore var friendId: Long) : RelBaseModel(personId, friendId)
{
    constructor() : this(0L,0L) // Dummy constructor for room, not nice but working
    companion object{ const val TABLE_NAME = "rel_person_friend" }
}
