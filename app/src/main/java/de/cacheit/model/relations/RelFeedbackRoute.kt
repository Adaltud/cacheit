package de.cacheit.model.relations

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.Index
import de.cacheit.model.Feedback
import de.cacheit.model.Person
import de.cacheit.model.route.Route

@Entity(tableName = RelFeedbackRoute.TABLE_NAME,
    foreignKeys = [ ForeignKey(entity = Feedback::class, parentColumns = ["id"], childColumns = ["parentId"]),
                    ForeignKey(entity = Route::class, parentColumns = ["id"], childColumns = ["childId"]) ],
    indices = [ Index(value = ["parentId"], unique = false), Index(value = ["childId"], unique = false) ] )
data class RelFeedbackRoute(@Ignore var feedbackId: Long, @Ignore var routeId: Long) : RelBaseModel(feedbackId, routeId)
{
    constructor() : this(0L,0L) // Dummy constructor for room, not nice but working
    companion object{ const val TABLE_NAME = "rel_feedback_route" }
}