package de.cacheit.model.relations.route

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.Index
import de.cacheit.model.Feedback
import de.cacheit.model.Person
import de.cacheit.model.relations.RelBaseModel
import de.cacheit.model.route.RoutePart

@Entity(tableName = RelRoutePartCreatedBy.TABLE_NAME,
    foreignKeys = [ ForeignKey(entity = RoutePart::class, parentColumns = ["id"], childColumns = ["parentId"]),
                    ForeignKey(entity = Person::class, parentColumns = ["id"], childColumns = ["childId"]) ],
    indices = [ Index(value = ["parentId"], unique = false), Index(value = ["childId"], unique = false) ] )
data class RelRoutePartCreatedBy(@Ignore var routePartId: Long, @Ignore var personId: Long) : RelBaseModel(routePartId, personId)
{
    constructor() : this(0L,0L) // Dummy constructor for room, not nice but working
    companion object{ const val TABLE_NAME = "rel_routepart_createdby" }
}