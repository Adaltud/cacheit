package de.cacheit.model

/**
 * Entities who inherit this Interface will have a property called "createdBy"
 *
 */
interface IEntityCreatedBy {

    /**
     * User who created this record.
     */
    var createdBy: Person?
}