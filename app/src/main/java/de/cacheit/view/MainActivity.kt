package de.cacheit.view

import android.Manifest
import android.content.pm.PackageManager
import android.nfc.NfcAdapter
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.model.Person
import de.cacheit.storage.LocalRemoteMapper
import de.cacheit.storage.remote.model.RPerson
import org.osmdroid.config.Configuration
import java.util.ArrayList


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    // Code to identify a request call.... obviously 42
    private val REQUEST_PERMISSIONS_REQUEST_CODE = 42

    /**
     * Permissions which are needed for the correct functionality of this app.
     */
    private val neededPermissions = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.INTERNET,
        Manifest.permission.ACCESS_NETWORK_STATE,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.CAMERA
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(setOf(
                R.id.navigation_home
        ))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        // this does hide the action bar in the MainActivity
        this.supportActionBar?.hide()

        // Request permissions for OSMDroid
        requestPermissionsIfNecessary(neededPermissions)

        // Set OSMDroid settings
        Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this))
        Configuration.getInstance().userAgentValue = this.packageName
        Configuration.getInstance().tileDownloadThreads = 12
    }

    override fun onStop() {
        super.onStop()
        NfcAdapter.getDefaultAdapter(this)?.disableReaderMode(this)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        var allPermissionsgranted = true
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            neededPermissions.forEach { permission ->
                if (!isPermissionGranted(permission))
                    allPermissionsgranted = false
            }
        }

        if (!allPermissionsgranted) {
            // Todo show message for user
            requestPermissionsIfNecessary(neededPermissions)
        }
    }

    private fun requestPermissionsIfNecessary(permissions: Array<String>) {
        val permissionsToRequest: ArrayList<String> = ArrayList()
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(this, permission)
                != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                permissionsToRequest.add(permission)
            }
        }

        // If the permissions are not granted, simply ask again.
        // This app cannot be used without those
        // We will be showing a nice message/activity for users in the near future.
        if (permissionsToRequest.size > 0) {
            ActivityCompat.requestPermissions(
                this,
                permissionsToRequest.toArray(arrayOfNulls(0)),
                REQUEST_PERMISSIONS_REQUEST_CODE)
        }
    }

    /**
     * Check if a specific permission has been granted.
     *
     * @param permission The permission to check.
     * @return True, if granted, otherwise false.
     */
    private fun isPermissionGranted(permission: String) : Boolean {
        return (ContextCompat.checkSelfPermission(this, permission)
                == PackageManager.PERMISSION_GRANTED)
    }
}