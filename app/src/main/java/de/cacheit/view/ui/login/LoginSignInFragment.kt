package de.cacheit.view.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.model.Person
import de.cacheit.view.MainActivity
import de.cacheit.view.ui.core.BaseFragment
import kotlinx.android.synthetic.main.fragment_login_signin.*
import kotlinx.android.synthetic.main.fragment_login_signup.*

@AndroidEntryPoint
class LoginSignInFragment : BaseFragment(R.layout.fragment_login_signin) {

    private val viewModel: LoginViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        signInButtonSignIn.setOnClickListener {
            val email = signInEditTextTextEmailAddress.text.toString()
            val password = signInEditTextTextPassword.text.toString()
            val person = viewModel.checkLogin(email, password)

            if (email == "" || password == "") {
                val toast =
                    Toast.makeText(
                        requireContext(),
                        "Bitte Alle Felder ausfüllen",
                        Toast.LENGTH_SHORT
                    )
                toast.show()
            } else if (person == null) {
                val toast =
                    Toast.makeText(
                        requireContext(),
                        "Nutzer nicht verfügbar",
                        Toast.LENGTH_SHORT
                    )
                toast.show()
            } else {
                loggedPerson = person
                startActivity(Intent(activity, MainActivity::class.java))
                activity?.finish()
            }

        }

        signInTextViewSignUp.setOnClickListener {
            val action = LoginSignInFragmentDirections.actionSignInFragmentToSignUpFragment()
            findNavController().navigate(action)
        }
    }
}