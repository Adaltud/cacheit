package de.cacheit.view.ui.core

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.cacheit.model.Person
import de.cacheit.model.route.Route
import java.util.*


/*
    Common super class for all our Fragments. Offers some regularly needed
    helper methods.
 */
abstract class BaseFragment(@LayoutRes contentLayoutId: Int) : Fragment(contentLayoutId) {

    private val REQUEST_PERMISSIONS_REQUEST_CODE = 1
    protected var locationManager : LocationManager? = null

    companion object{
        var loggedPerson : Person = Person()
    }

    protected fun hideBackButton() {
        // Hide Back Button
        Objects.requireNonNull(
                (requireActivity() as AppCompatActivity).supportActionBar
        )?.setDisplayHomeAsUpEnabled(false)
    }

    protected fun <T : ViewModel?> getViewModel(tClass: Class<T>?): T {
        return ViewModelProvider(
                this,
                ViewModelProvider.AndroidViewModelFactory.getInstance(
                        requireActivity().application
                )
        ).get(tClass as Class<T>)
    }

    /*
        Helper method to hide the keyboard, for example when submitting a form.
     */
    protected fun hideKeyboard(context: Context, view: View) {
        val imm: InputMethodManager =
                context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        val permissionsToRequest: ArrayList<String?> = ArrayList()
        for (i in grantResults.indices) {
            permissionsToRequest.add(permissions[i])
        }
        if (permissionsToRequest.size > 0) {
            ActivityCompat.requestPermissions(
                    this.requireActivity(),
                    permissionsToRequest.toArray(arrayOfNulls(0)),
                    REQUEST_PERMISSIONS_REQUEST_CODE)
        }
    }

    protected fun requestPermissionsIfNecessary(permissions: Array<String>) {
        val permissionsToRequest: ArrayList<String> = ArrayList()
        for (permission in permissions) {
            if (!isPermissionGranted(permission)) {
                // Permission is not granted
                permissionsToRequest.add(permission)
            }
        }
        if (permissionsToRequest.size > 0) {
            ActivityCompat.requestPermissions(
                    this.requireActivity(),
                    permissionsToRequest.toArray(arrayOfNulls(0)),
                    REQUEST_PERMISSIONS_REQUEST_CODE)
        }
    }

    protected fun isPermissionGranted(permission: String) : Boolean {
        return (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED)
    }
}
