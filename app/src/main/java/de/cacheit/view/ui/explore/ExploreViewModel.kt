package de.cacheit.view.ui.explore

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import de.cacheit.model.route.Cache
import de.cacheit.model.route.Coordinates
import de.cacheit.model.route.Route
import de.cacheit.model.route.RoutePart
import de.cacheit.storage.local.dao.route.CoordinatesDao
import de.cacheit.storage.repositories.route.CacheRepository
import de.cacheit.storage.repositories.route.CoordinatesRepository
import de.cacheit.storage.repositories.route.RoutePartRepository
import de.cacheit.storage.repositories.route.RouteRepository
import kotlinx.coroutines.runBlocking
import java.lang.Exception

class ExploreViewModel @ViewModelInject constructor(
    private val cacheRepo: CacheRepository,
    private val routePartRepo: RoutePartRepository,
    private val routeRepo: RouteRepository,
    private val coordinatesRepo: CoordinatesRepository

) : ViewModel() {

    val caches = runBlocking { cacheRepo.getAllLiveAwait() }
    val routeParts = runBlocking { routePartRepo.getAllLiveAwait() }
    val routes = runBlocking { routeRepo.getAllLiveAwait() }

    var route: Route? = null
    var routePart: RoutePart? = null
    var cache: Cache? = null


// Variablen für Listen von Elementen Anlegen

    fun saveCoordinates(coordinates: Coordinates): Boolean {
        val id: Long
        runBlocking { id = coordinatesRepo.insertComplexAutoAwait(coordinates) }
        return id != 0L
    }

    fun saveCache(cache: Cache): Boolean {
        val id: Long
        runBlocking { id = cacheRepo.insertComplexAutoAwait(cache) }
        return id != 0L
    }

    fun getCacheComplex(id: Long): Cache? {
        return runBlocking { cacheRepo.getByIdComplexAutoAwait(id) }
    }

    fun getRoutePartComplex(id: Long): RoutePart? {
        return runBlocking { routePartRepo.getByIdComplexAutoAwait(id) }
    }

    fun getRouteComplex(id: Long): Route? {
        return runBlocking { routeRepo.getByIdComplexAutoAwait(id) }
    }


    fun getCache(cacheID: Long): Cache? {
        return runBlocking { cacheRepo.getByIdComplexAutoAwait(cacheID) }
    }

    fun saveRoutePart(routePart: RoutePart): Boolean {
        var id = 0L
        try {
            runBlocking { id = routePartRepo.insertComplexAutoAwait(routePart) }
        } catch (ex: Exception) {
            ex.message
        }

        return id != 0L
    }

    fun updateRoute(id: Long, route: Route): Boolean {

        return if (getRouteComplex(id) != null) {
            try {
                runBlocking { routeRepo.updateComplexAutoAwait(route) }
            } catch (ex: Exception) {
                ex.message
            }
            true
        } else {
            // TODO Route nicht vorhanden
            false
        }
    }

    fun getRoutePart(routePartID: Long): RoutePart? {
        return runBlocking { routePartRepo.getByIdComplexAutoAwait(routePartID) }
    }

    fun saveRoute(route: Route): Boolean {
        val id: Long
        runBlocking { id = routeRepo.insertComplexAutoAwait(route) }
        return id != 0L
    }


}
