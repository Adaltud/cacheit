package de.cacheit.view.ui.explore.routePart

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.iterator
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.databinding.FragmentRoutepartAddEditBinding
import de.cacheit.model.challenge.TextChallenge
import de.cacheit.model.route.Cache
import de.cacheit.model.route.RoutePart
import de.cacheit.view.ui.core.BaseFragment
import de.cacheit.view.ui.explore.ExploreViewModel
import de.cacheit.view.ui.explore.cache.CachesAdapter
import kotlinx.android.synthetic.main.fragment_routepart_add_edit.*
import kotlinx.android.synthetic.main.item_cache.view.*

@AndroidEntryPoint
class RoutePartAddEditFragment : BaseFragment(R.layout.fragment_routepart_add_edit),
    CachesAdapter.OnItemClickListener {

    private val viewModel: ExploreViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentRoutepartAddEditBinding.bind(view)
        val cacheAdapter = CachesAdapter(this)

        // Binds the RecyclerView for the Unlock Cache
        binding.apply {
            addEditRoutePartRecyclerViewUnlockCache.apply {
                adapter = cacheAdapter
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
            }
        }

        // Binds the RecyclerView for the Finish Cache
        binding.apply {
            addEditRoutePartRecyclerViewFinishCache.apply {
                adapter = cacheAdapter
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
            }
        }

        // Observes the Caches, which the Viewmodel gets as LiveData
        viewModel.caches.observe(viewLifecycleOwner) {
            cacheAdapter.submitList(it)
        }

        routePart_fltbtn_check.setOnClickListener {
            val name = add_edit_routePart_editText_name.text.toString()
            val description = add_edit_routePart_editText_description.text.toString()


/*          TODO
            //get the text of the selected challenge type
            var challenge = radioGroup.findViewById<RadioButton>(radioGroup.checkedRadioButtonId).text.toString()
*/
                val challenge = TextChallenge("Platzhalter")


                // gets the index of the cache were the Box is checked
                var cacheToUnlockPart = Cache()
                for (i in add_edit_routePart_recyclerView_unlock_cache.iterator()) {
                    if (i.cache_check_box.isChecked) {
                        cacheToUnlockPart = cacheAdapter.getItem(
                            add_edit_routePart_recyclerView_unlock_cache.getChildLayoutPosition(i)
                        )
                        break
                    }
                }

                // gets the index of the cache were the Box is checked
                var cacheToFinishPart = Cache()
                for (i in add_edit_routePart_recyclerView_finish_cache.iterator()) {
                    if (i.cache_check_box.isChecked) {
                        cacheToFinishPart = cacheAdapter.getItem(
                            add_edit_routePart_recyclerView_finish_cache.getChildLayoutPosition(i)
                        )
                        break
                    }
                }

            if (name.isEmpty() || description.isEmpty() || cacheToFinishPart.id == 0L || cacheToUnlockPart.id == 0L) {
                Toast.makeText(requireContext(), "Every field needs to be set.", Toast.LENGTH_SHORT).show()
            }
            else {
                val routePart =
                    RoutePart(
                        name,
                        false,
                        description,
                        challenge,
                        cacheToUnlockPart,
                        cacheToFinishPart
                    )

                if (viewModel.saveRoutePart(routePart)) {
                    val action =
                        RoutePartAddEditFragmentDirections.actionRoutePartAddEditFragmentToExploreLandingFragment()
                    findNavController().navigate(action)
                }
            }
        }
    }

    override fun onItemClick(cache: Cache) {
        val action =
            RoutePartAddEditFragmentDirections.actionRoutePartAddEditFragmentToCacheDetailFragment(
                cache
            )
        findNavController().navigate(action)
    }
}