package de.cacheit.view.ui.explore.routePart

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.databinding.FragmentRoutepartsBinding
import de.cacheit.model.route.RoutePart
import de.cacheit.view.ui.core.BaseFragment
import de.cacheit.view.ui.explore.ExploreLandingFragmentDirections
import de.cacheit.view.ui.explore.ExploreViewModel
import kotlinx.android.synthetic.main.fragment_routeparts.*


@AndroidEntryPoint
class RoutePartsFragment : BaseFragment(R.layout.fragment_routeparts), RoutePartsAdapter.OnItemClickListener {

    private val viewModel: ExploreViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentRoutepartsBinding.bind(view)

        val routePartAdapter = RoutePartsAdapter(this)

        binding.apply {
            routePartsRecyclerView.apply {
                adapter = routePartAdapter
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
            }
        }

        viewModel.routeParts.observe(viewLifecycleOwner) {
            routePartAdapter.submitList(it)
        }

        routeParts_fltbtn_new.setOnClickListener {
            val action = ExploreLandingFragmentDirections.actionExploreLandingFragmentToRoutePartAddEditFragment()
            findNavController().navigate(action)
        }


    }

    override fun onItemClick(routePart: RoutePart) {
        val action = ExploreLandingFragmentDirections.actionExploreLandingFragmentToRoutePartDetailFragment(routePart)
        findNavController().navigate(action)
    }

}