package de.cacheit.view.ui.explore.route

import android.content.Intent
import android.nfc.NfcAdapter
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.zxing.integration.android.IntentIntegrator
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.model.route.Cache
import de.cacheit.model.route.Route
import de.cacheit.model.route.RoutePart
import de.cacheit.utils.nfc.NFCUtil
import de.cacheit.utils.osmdroid.OSMDroidUtil
import de.cacheit.view.ui.core.BaseFragment
import de.cacheit.view.ui.explore.ExploreViewModel
import de.cacheit.view.ui.qrscan.DefaultPortraitQRCaptureActivity
import kotlinx.android.synthetic.main.fragment_route_play.*
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import java.lang.Exception

@AndroidEntryPoint
class RoutePlayFragment : BaseFragment(R.layout.fragment_route_play) {

    private val args: RoutePlayFragmentArgs by navArgs()
    private val viewModel: ExploreViewModel by viewModels()
    private var route: Route? = null
    private var activeRoutePart: RoutePart? = null
    private lateinit var routePartIterator: MutableListIterator<RoutePart>
    private var routestartet: Boolean = false

    var mMapView: MapView? = null
    private var nfcAdapter: NfcAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        route = viewModel.getRouteComplex(args.route.id)
        routePartIterator = route?.routeParts!!.listIterator()

        if (routePartIterator.hasNext()) {

            activeRoutePart = viewModel.getRoutePartComplex(routePartIterator.next().id)
            updateView(activeRoutePart?.cacheToUnlock)
        }

        route_play_fltbtn_nfc_code.setOnClickListener {
            if (nfcAdapter == null)
                nfcAdapter = NfcAdapter.getDefaultAdapter(requireContext())

            nfcAdapter?.enableReaderMode(
                activity,
                {
                    scan(NFCUtil().getIdString(it), false)
                    nfcAdapter?.disableReaderMode(activity)
                },
                NfcAdapter.FLAG_READER_NFC_A or NfcAdapter.FLAG_READER_NFC_B or NfcAdapter.FLAG_READER_NFC_F or NfcAdapter.FLAG_READER_NFC_V or NfcAdapter.FLAG_READER_NFC_BARCODE,
                null
            )
        }

        route_play_fltbtn_qr_code.setOnClickListener {
            nfcAdapter?.disableReaderMode(activity)

            val integrator = IntentIntegrator.forSupportFragment(this)
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
            integrator.setBeepEnabled(false)
            integrator.setBarcodeImageEnabled(true)
            integrator.captureActivity = DefaultPortraitQRCaptureActivity::class.java
            integrator.setOrientationLocked(true)
            integrator.initiateScan()
        }

        route_play_btn_skip.setOnClickListener {
            scan("skip", true)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
            if (result != null) {
                scan(result.contents, false)
            } else {
                super.onActivityResult(requestCode, resultCode, data)
            }
        } catch (ex: Exception) {
            Log.e("QR", "Unable to scan QR-Code-Tag.", ex)
        }
    }

    private fun scan(code: String, skip: Boolean): Boolean {

        if (!routestartet) {
            if (skip && routePartIterator.hasNext())
                updateView(activeRoutePart?.cacheToFinishPart)
            routestartet = true
            return true
        }

        // If skip button is pushed and there is a RoutePart left Update the View
        if (skip && routePartIterator.hasNext()) {
            activeRoutePart = viewModel.getRoutePartComplex(routePartIterator.next().id)
            updateView(activeRoutePart?.cacheToFinishPart)
        }

        // Else go back to ExploreLanding Page
        else if (!routePartIterator.hasNext()) {
            val action =
                RoutePlayFragmentDirections.actionRoutePlayFragmentToExploreLandingFragment()
            findNavController().navigate(action)
        }

        // if there is another Routepart
        if (routePartIterator.hasNext()) {
            // If the codes match
            if (code == activeRoutePart?.cacheToFinishPart?.NFCCode) {
                //Go to next Routepart
                activeRoutePart = viewModel.getRoutePartComplex(routePartIterator.next().id)
                updateView(activeRoutePart?.cacheToFinishPart)
                return true
            } else {
                // TODO Toast Message: Wrong Code/Cache
                return false
            }
        } else {
            //TODO Toast Route finished + Save
        }
        return false
    }

    private fun updateView(cache: Cache?) {
        var tCache: Cache? = cache
        mMapView?.overlayManager?.clear()

        route_play_active_cache.text = tCache?.name

        // Update the Map
        if (mMapView == null)
            mMapView = view?.findViewById(R.id.route_play_mapview)

        if (tCache?.coordinates == null)
            tCache = viewModel.getCacheComplex(cache!!.id)

        // calcu
        val coordFinish = GeoPoint(
            tCache?.coordinates!!.latitude,
            tCache?.coordinates!!.longitude
        )

        // Show pins on the map
        OSMDroidUtil().placeSimpleMarker(
            mMapView!!,
            coordFinish,
            tCache.name
        )

        mMapView?.controller?.setZoom(17)
        mMapView?.controller?.setCenter(GeoPoint(coordFinish.latitude, coordFinish.longitude))

        // recreate map
        mMapView?.invalidate()
    }

}