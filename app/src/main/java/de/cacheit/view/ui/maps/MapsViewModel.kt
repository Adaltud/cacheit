package de.cacheit.view.ui.maps

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import de.cacheit.model.route.Cache
import de.cacheit.model.route.Coordinates
import de.cacheit.model.route.Route
import de.cacheit.model.route.RoutePart
import de.cacheit.storage.repositories.route.CacheRepository
import de.cacheit.storage.repositories.route.CoordinatesRepository
import de.cacheit.storage.repositories.route.RoutePartRepository
import de.cacheit.storage.repositories.route.RouteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MapsViewModel @ViewModelInject constructor(
    private val routeRepo: RouteRepository,
    private val routePartRepo: RoutePartRepository,
    private val coordinatesRepo: CoordinatesRepository

) : ViewModel() {

    fun getRoutesInBound(latitudeLowerBound: Double, latitudeUpperBound: Double,
                         longitudeLowerBound: Double, longitudeUpperBound: Double, limit: Int = 50) : List<Route> {
        return runBlocking {  withContext(Dispatchers.Default) { routeRepo.getRoutesInBoundAwait(latitudeLowerBound, latitudeUpperBound, longitudeLowerBound, longitudeUpperBound, limit) }}
    }

    fun getStartRoutePartComplex(id: Long): RoutePart? {
        return runBlocking {  routePartRepo.getByIdComplexAutoAwait(routeRepo.getStartRoutePartIdAwait(id)) }
    }

    fun getCoordinatesById(id: Long): Coordinates? {
        return runBlocking {  coordinatesRepo.getByIdAwait(id) }
    }
}
