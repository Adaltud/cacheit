package de.cacheit.view.ui.explore.cache

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.model.route.Cache
import de.cacheit.utils.osmdroid.OSMDroidUtil
import de.cacheit.view.ui.core.BaseFragment
import de.cacheit.view.ui.explore.ExploreViewModel
import kotlinx.android.synthetic.main.fragment_cache_detail.*
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView

@AndroidEntryPoint
class CacheDetailFragment : BaseFragment(R.layout.fragment_cache_detail){

    private val args: CacheDetailFragmentArgs by navArgs()
    private val viewModel: ExploreViewModel by viewModels()
    var mMapView: MapView? = null

    override fun onResume() {
        super.onResume()
        updateView(args.cache)
    }

    private fun updateView(cache: Cache?) {

        if (cache == null)
            return

        viewModel.cache = cache

        if (viewModel.cache!!.coordinates == null)
            viewModel.cache = viewModel.getCacheComplex(cache.id)

        cache_detail_name.text = viewModel.cache?.name
        cache_detail_description.text = viewModel.cache?.description
        cache_detail_nfc_code.text = viewModel.cache?.NFCCode.toString()
        cache_detail_latitude.text = viewModel.cache?.coordinates?.latitude.toString()
        cache_detail_longitute.text = viewModel.cache?.coordinates?.longitude.toString()

        // Update the Map
        mMapView = view?.findViewById(R.id.cache_detail_mapview)

        // Check coordinates
        val coords = GeoPoint(viewModel.cache?.coordinates!!.latitude, viewModel.cache?.coordinates!!.longitude)

        // Show pins on the map
        OSMDroidUtil().placeSimpleMarker(mMapView!!, coords, viewModel.cache!!.name)

        mMapView?.addOnFirstLayoutListener(MapView.OnFirstLayoutListener { _, _, _, _, _ ->

            // Zoom and scroll to Cache point
            mMapView?.controller?.setZoom(17)
            mMapView?.controller?.setCenter(GeoPoint(coords.latitude, coords.longitude))

            // recreate map
            mMapView?.invalidate()
        })
    }

}