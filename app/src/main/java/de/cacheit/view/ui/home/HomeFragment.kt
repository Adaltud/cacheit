package de.cacheit.view.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.view.ui.core.BaseFragment

@AndroidEntryPoint
class HomeFragment : BaseFragment(R.layout.fragment_home){

    /**
     * Shows the dashboard, in the next versions, it inform about, what routes friends hunt
     * or feedback friends recommment
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        this.hideBackButton()


        return super.onCreateView(inflater, container, savedInstanceState)
    }

}