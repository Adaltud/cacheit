package de.cacheit.view.ui.explore.cache

import android.content.ClipData
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import de.cacheit.databinding.ItemCacheBinding
import de.cacheit.model.route.Cache

class CachesAdapter(private val listener: OnItemClickListener) : ListAdapter<Cache,CachesAdapter.CachesViewHolder>(DiffCallBack()){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CachesViewHolder {
        val binding = ItemCacheBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CachesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CachesViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    public override fun getItem(position: Int): Cache {
        return super.getItem(position)
    }

    inner class CachesViewHolder(private val binding: ItemCacheBinding) :
        RecyclerView.ViewHolder(binding.root),
            View.OnClickListener {

        fun bind(cache: Cache) {
            binding.apply {
                cacheCheckBox.isChecked = false
                cacheTextCachename.text = cache.name
            }
        }

        init {
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val cache : Cache = getItem(adapterPosition)
            listener.onItemClick(cache)
        }
    }

    interface OnItemClickListener {
        fun onItemClick(cache: Cache)
    }

    class DiffCallBack : DiffUtil.ItemCallback<Cache>(){

        override fun areItemsTheSame(oldItem: Cache, newItem: Cache) =              // Same as" : Boolean { return oldItem.id == newItem.id} "
            oldItem.id  == newItem.id

        override fun areContentsTheSame(oldItem: Cache, newItem: Cache) =           // Same as" : Boolean { return oldItem == newItem} "
            oldItem == newItem

    }
}