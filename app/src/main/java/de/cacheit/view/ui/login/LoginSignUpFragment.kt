package de.cacheit.view.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.model.Person
import de.cacheit.view.MainActivity
import de.cacheit.view.ui.core.BaseFragment

import kotlinx.android.synthetic.main.fragment_login_signup.*

@AndroidEntryPoint
class LoginSignUpFragment : BaseFragment(R.layout.fragment_login_signup) {

    private val viewModel: LoginViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        signUpButtonSignUp.setOnClickListener {

            val username = signUpEditTextTextUsername.text.toString()
            val email = signUpEditTextTextEmailAddress.text.toString()
            val password = signUpEditTextTextPassword.text.toString()
            val passwordRepeat = signUpEditTextTextPasswordrepeat.text.toString()

            if (password == passwordRepeat ) {

                if(username != "" && email != "" && password != "" && passwordRepeat != "") {
                    val newPerson = Person(username, email, password)
                    loggedPerson = newPerson
                    viewModel.savePerson(newPerson)

                    startActivity(Intent(activity, MainActivity::class.java))
                    activity?.finish()
                }
                else{
                    val toast =
                        Toast.makeText(requireContext(), "Bitte Alle Felder ausfüllen", Toast.LENGTH_SHORT)
                    toast.show()
                }
            } else {
                val toast =
                    Toast.makeText(requireContext(), "Passwords do not match", Toast.LENGTH_SHORT)
                toast.show()
            }
        }

        textViewSignUpGoToSignIn.setOnClickListener {
            val action = LoginSignUpFragmentDirections.actionSignUpFragmentToSignInFragment()
            findNavController().navigate(action)
        }

    }

}