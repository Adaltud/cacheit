package de.cacheit.view.ui.explore

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.model.route.Route
import de.cacheit.view.ui.core.BaseFragment
import de.cacheit.view.ui.explore.cache.CachesFragment
import de.cacheit.view.ui.explore.route.RoutesFragment
import de.cacheit.view.ui.explore.routePart.RoutePartsFragment
import kotlinx.android.synthetic.main.fragment_explore_landing.*

@AndroidEntryPoint
class ExploreLandingFragment : BaseFragment(R.layout.fragment_explore_landing) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpTabs()
    }

    private fun setUpTabs() {
        val adapter = ExploreViewPagerAdapter(childFragmentManager)
        adapter.addFragment(RoutesFragment(), "Routes")
        adapter.addFragment(RoutePartsFragment(), "Routeparts")
        adapter.addFragment(CachesFragment(), "Caches")
        exlore_viewpager.adapter = adapter

        explore_tabs.setupWithViewPager(exlore_viewpager)
        explore_tabs.getTabAt(0)!!.setIcon(R.drawable.ic_explore_black_outlined_24px)
        explore_tabs.getTabAt(1)!!.setIcon(R.drawable.ic_place_black_outlined_24px)
        explore_tabs.getTabAt(2)!!.setIcon(R.drawable.ic_baseline_nfc_24)

    }

}