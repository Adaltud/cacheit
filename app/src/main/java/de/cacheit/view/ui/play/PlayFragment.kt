package de.cacheit.view.ui.play

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.cacheit.R
import de.cacheit.view.ui.core.BaseFragment

class PlayFragment : BaseFragment(R.layout.fragment_play){

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.hideBackButton()


    }

}