package de.cacheit.view.ui.profil


import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.databinding.FragmentProfilEditBinding
import de.cacheit.model.Person
import de.cacheit.view.ui.core.BaseFragment
import kotlinx.android.synthetic.main.fragment_cache_add_edit.*
import kotlinx.android.synthetic.main.fragment_profil_edit.*

@AndroidEntryPoint
class ProfilEditFragment : BaseFragment(R.layout.fragment_profil_edit) {

    private val viewModel: ProfilViewModel by viewModels()
    val save = "Save new Data"
    val nSave = "Nothing to save"
    val falsePw = "the passwords is not the same"
    val niOEmail = "The email isn't valid"
    val duration = Toast.LENGTH_SHORT

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentProfilEditBinding.bind(view)

        profil_edit_text_show_username.text = loggedPerson.userName

        /**
         * Goto the profile page
         */

        profil_edit_btn_edit_profil.setOnClickListener {

            val newUsername = profil_edit_edit_text_username.text.toString()
            val newEMail = profil_edit_edit_text_email.text.toString()
            val newPassword = profil_edit_edit_text_password.text.toString()
            val newPasswordRepeat = profil_edit_edit_text_repeat_password.text.toString()

            val passwordNew: Boolean = (newPassword == newPasswordRepeat && newPassword != "")
            val eMailNew: Boolean = newEMail.isEmailValid()
            val usernameNew: Boolean = newUsername != ""
            val newPerson = loggedPerson

            if (usernameNew) {
                newPerson.userName = newUsername
            }
            if (eMailNew) {
                newPerson.eMail = newEMail
            } else if (!eMailNew && newEMail != "") {
                Toast.makeText(requireContext(), niOEmail, duration).show()
            }
            if (passwordNew) {
                newPerson.password = newPassword
            } else if (!passwordNew && (newPassword != "" || newPasswordRepeat != "")) {
                Toast.makeText(requireContext(), "Passwords do not match", duration).show()
            }
            if (passwordNew || eMailNew || usernameNew) {
                // Aktualisiere die Person
                updatePerson(newPerson)
                val action =
                    ProfilEditFragmentDirections.actionProfilEditFragmentToProfilLandingFragment()
                findNavController().navigate(action)
            }
            if (newUsername =="" && newEMail =="" && newPassword =="" && newPasswordRepeat =="" ) {
                Toast.makeText(requireContext(), "No Changes", duration).show()
                val action =
                    ProfilEditFragmentDirections.actionProfilEditFragmentToProfilLandingFragment()
                findNavController().navigate(action)
            }
        }
    }


    override fun onResume() {
        super.onResume()
        // updateView(person)
    }

    /**
     * validate the email
     */
    fun String.isEmailValid(): Boolean {
        return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this)
            .matches()
    }

    private fun updatePerson(person: Person) {
        viewModel.updatePerson(person.id, person)
        loggedPerson = viewModel.getPersonComplex(person.id)!!
    }

    private fun updateView(person: Person?) {

        // Alle textfelder etc setzten, wie in RouteDetail
        // Set fields

        if (person == null)
            return

        viewModel.person = person

        if (viewModel.person?.userName == null)
            viewModel.person = viewModel.getPersonComplex(person.id)


//        person_edit_username.text =  viewModel.person?.userName
//        person__edit_email.text =  viewModel.person?.eMail
//        profil_edit_password.text = viewModel.person?.password

    }
}