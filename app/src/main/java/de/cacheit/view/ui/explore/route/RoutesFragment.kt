package de.cacheit.view.ui.explore.route

import android.os.Bundle
import android.view.View 
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.databinding.FragmentRoutesBinding
import de.cacheit.model.route.Route
import de.cacheit.view.ui.core.BaseFragment
import de.cacheit.view.ui.explore.ExploreLandingFragmentDirections
import de.cacheit.view.ui.explore.ExploreViewModel
import kotlinx.android.synthetic.main.fragment_routes.*

@AndroidEntryPoint
class RoutesFragment : BaseFragment(R.layout.fragment_routes), RoutesAdapter.OnItemClickListener {

    private val viewModel: ExploreViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentRoutesBinding.bind(view)
        val routeAdapter = RoutesAdapter(this)

        binding.apply {
            routesRecyclerView.apply {
                adapter = routeAdapter
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
            }
        }

        viewModel.routes.observe(viewLifecycleOwner) {
            routeAdapter.submitList(it)
        }

        routes_fltbtn_new.setOnClickListener {
            val action = ExploreLandingFragmentDirections.actionExploreLandingFragmentToRouteAddEditFragment()
            findNavController().navigate(action)
        }

    }

    override fun onItemClick(route: Route) {
        val action = ExploreLandingFragmentDirections.actionExploreLandingFragmentToRouteDetailFragment(route)
        findNavController().navigate(action)
    }
}