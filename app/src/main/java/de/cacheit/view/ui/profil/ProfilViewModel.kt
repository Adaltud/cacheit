package de.cacheit.view.ui.profil
import android.text.TextUtils
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import de.cacheit.model.Person
import de.cacheit.model.route.Route
import de.cacheit.model.route.RoutePart
import de.cacheit.storage.repositories.PersonRepository
import de.cacheit.storage.repositories.route.CacheRepository
import de.cacheit.storage.repositories.route.RoutePartRepository
import de.cacheit.storage.repositories.route.RouteRepository
import kotlinx.coroutines.runBlocking
import java.lang.Exception

class ProfilViewModel @ViewModelInject constructor(
    private val personRepo: PersonRepository,
    private val routeRepo: RouteRepository,
    private val routePartRepo: RoutePartRepository,
    private val cacheRepo: CacheRepository

) : ViewModel() {

    var person: Person? = null


    /**
     * get Person
     */
    fun getPersonComplex(id: Long): Person? {
        return runBlocking {  personRepo.getByIdComplexAutoAwait(id) }
    }


    fun savePerson(person: Person): Boolean {
        val id: Long
        runBlocking { id = personRepo.insertComplexAutoAwait(person) }
        return id != 0L
    }

    /**
     * update person, to change email or password
     */
    fun updatePerson(id: Long, person: Person): Boolean {

        return if (getPersonComplex(id) != null) {
            try {
                runBlocking { personRepo.updateComplexAutoAwait(person) }
            } catch (ex: Exception) {
                ex.message
            }
            true
        } else {
            // TODO Person nicht vorhanden
            false
        }
    }



}
