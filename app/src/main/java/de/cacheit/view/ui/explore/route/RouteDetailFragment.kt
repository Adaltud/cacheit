package de.cacheit.view.ui.explore.route

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.model.route.Route
import de.cacheit.utils.osmdroid.OSMDroidUtil
import de.cacheit.view.ui.core.BaseFragment
import de.cacheit.view.ui.explore.ExploreViewModel
import kotlinx.android.synthetic.main.fragment_route_detail.*
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView

@AndroidEntryPoint
class RouteDetailFragment : BaseFragment(R.layout.fragment_route_detail){

    private val args: RouteDetailFragmentArgs by navArgs()
    private val viewModel: ExploreViewModel by viewModels()
    var mMapView: MapView? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        route_detail_button_play_route.setOnClickListener{
            val action = RouteDetailFragmentDirections.actionRouteDetailFragmentToRoutePlayFragment(args.route)
            findNavController().navigate(action)
        }

        route_detail_button_add_routepart.setOnClickListener {
            val action = RouteDetailFragmentDirections.actionRouteDetailFragmentToRouteAddRoutePartFragment(args.route)
            findNavController().navigate(action)
        }
    }

    override fun onResume() {
        super.onResume()
        updateView(args.route)
    }

    private fun updateView(route: Route?) {

        if (route == null)
            return

        viewModel.route = route

        // Get RouteParts if they are not already set
        if (viewModel.route!!.routeParts == null)
            viewModel.route = viewModel.getRouteComplex(route.id)

        // Set fields
        route_detail_name.text = viewModel.route?.name
        route_detail_description.text = viewModel.route?.description
        route_detail_rating.text = viewModel.route?.rating.toString()
        route_detail_solvedcounter.text = viewModel.route?.solvedCounter.toString()
        route_detail_viewcounter.text = viewModel.route?.viewCounter.toString()

        // Update the Map
        if (mMapView == null)
            mMapView = view?.findViewById(R.id.route_detail_mapview)

        // Get caches for each routepart
        val iterate = viewModel.route?.routeParts!!.listIterator()
        while (iterate.hasNext()) {
            var cur = iterate.next()
            if (cur.cacheToUnlock == null || cur.cacheToFinishPart == null)
                iterate.set(viewModel.getRoutePartComplex(cur.id)!!)
        }

        var geoPoints: MutableList<GeoPoint> = mutableListOf()
        val cacheIterator = viewModel.route?.routeParts!!.listIterator()
        while (cacheIterator.hasNext()) {
            var cur = cacheIterator.next()
            if (cur.cacheToUnlock == null || cur.cacheToFinishPart == null)
                cur = viewModel.getRoutePartComplex(cur.id)!!

            if (cur.cacheToUnlock?.coordinates == null)
                cur.cacheToUnlock = viewModel.getCacheComplex(cur.cacheToUnlock!!.id)

            if (cur.cacheToFinishPart?.coordinates == null)
                cur.cacheToFinishPart = viewModel.getCacheComplex(cur.cacheToFinishPart!!.id)

            // Check coordinates
            val coordUnlock = GeoPoint(
                cur.cacheToUnlock?.coordinates!!.latitude,
                cur.cacheToUnlock?.coordinates!!.longitude
            )
            val coordFinish = GeoPoint(
                cur.cacheToFinishPart?.coordinates!!.latitude,
                cur.cacheToFinishPart?.coordinates!!.longitude
            )

            // Show pins on the map
            OSMDroidUtil().placeSimpleMarker(
                mMapView!!,
                coordUnlock,
                cur.cacheToUnlock!!.name
            )
            OSMDroidUtil().placeSimpleMarker(
                mMapView!!,
                coordFinish,
                cur.cacheToFinishPart!!.name
            )

            geoPoints.addAll(arrayOf(coordFinish, coordUnlock))
        }

        var bbox = OSMDroidUtil().getBoundingBox(geoPoints)
        mMapView?.addOnFirstLayoutListener(MapView.OnFirstLayoutListener { _, _, _, _, _ ->
            // Zoom and scroll to Cache points
            mMapView?.zoomToBoundingBox(bbox, false)

            // Zoom out (only 1 time)
            mMapView?.controller?.zoomOut()

            // recreate map
            mMapView?.invalidate()
        })

    }


}