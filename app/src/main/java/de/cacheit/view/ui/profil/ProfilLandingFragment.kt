package de.cacheit.view.ui.profil
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.cacheit.R
import de.cacheit.databinding.FragmentProfilLandingBinding
import de.cacheit.model.Person
import de.cacheit.view.ui.core.BaseFragment
import de.cacheit.view.ui.explore.route.RouteDetailFragmentArgs
import kotlinx.android.synthetic.main.fragment_profil_landing.*

class ProfilLandingFragment : BaseFragment(R.layout.fragment_profil_landing) {

    private val args: ProfilLandingFragment by navArgs()
    private val viewModel: ProfilViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentProfilLandingBinding.bind(view)

        updateView(loggedPerson)

        profil_btn_edit.setOnClickListener {
            val action = ProfilLandingFragmentDirections.actionProfilLandingFragmentToProfilEditFragment()
            findNavController().navigate(action)
        }

    }

    private fun updateView(person: Person?) {

        // Alle textfelder etc setzten, wie in RouteDetail
        // Set fields
        if (person != null) {
            profil_text_username.text = person.userName
            profil_edit_email.text =  person.eMail
        }
        else{
            profil_text_username.text = "Kein Nutzer eingeloggt"
            profil_edit_email.text =  "Kein Nutzer eingeloggt"

            val toast =
                Toast.makeText(
                    requireContext(),
                    "Kein Nutzer eingeloggt",
                    Toast.LENGTH_SHORT
                )
            toast.show()
        }




      //  if (loggedPerson == null)
       //     loggedPerson = p




    }

}