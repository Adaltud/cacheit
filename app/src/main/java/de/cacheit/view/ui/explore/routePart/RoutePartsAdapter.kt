package de.cacheit.view.ui.explore.routePart

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import de.cacheit.databinding.ItemRoutepartBinding
import de.cacheit.model.route.RoutePart
import de.cacheit.view.ui.explore.cache.CachesAdapter

class RoutePartsAdapter(private val listener: OnItemClickListener) : ListAdapter<RoutePart,RoutePartsAdapter.RoutePartsViewHolder>(DiffCallBack()){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoutePartsViewHolder {
        val binding = ItemRoutepartBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RoutePartsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RoutePartsViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class RoutePartsViewHolder(private val binding: ItemRoutepartBinding) : RecyclerView.ViewHolder(binding.root),
        View.OnClickListener{
        fun bind(routePart: RoutePart) {
            binding.apply {
                routePartCheckBox.isChecked = false
                routePartTextRoutePartname.text = routePart.name
            }
        }

        init {
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val routePart : RoutePart = getItem(adapterPosition)
            listener.onItemClick(routePart)
        }
    }

    public override fun getItem(position: Int): RoutePart {
        return super.getItem(position)
    }

    interface OnItemClickListener {
        fun onItemClick(routePart: RoutePart)
    }

    class DiffCallBack : DiffUtil.ItemCallback<RoutePart>(){

        override fun areItemsTheSame(oldItem: RoutePart, newItem: RoutePart) =              // Same as" : Boolean { return oldItem.id == newItem.id} "
            oldItem.id  == newItem.id

        override fun areContentsTheSame(oldItem: RoutePart, newItem: RoutePart) =           // Same as" : Boolean { return oldItem == newItem} "
            oldItem == newItem

    }
}