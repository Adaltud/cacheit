package de.cacheit.view.ui.explore.routePart

import android.Manifest
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.model.route.Coordinates
import de.cacheit.model.route.RoutePart
import de.cacheit.utils.osmdroid.OSMDroidUtil
import de.cacheit.view.ui.core.BaseFragment
import de.cacheit.view.ui.explore.ExploreViewModel
import kotlinx.android.synthetic.main.fragment_routepart_detail.*
import org.osmdroid.util.BoundingBox
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.MapView.OnFirstLayoutListener
import org.osmdroid.views.overlay.Marker

@AndroidEntryPoint
class RoutePartDetailFragment : BaseFragment(R.layout.fragment_routepart_detail) {
    private val args: RoutePartDetailFragmentArgs by navArgs()
    private val viewModel: ExploreViewModel by viewModels()
    var mMapView: MapView? = null

    override fun onResume() {
        super.onResume()
        updateView(args.routePart)
    }

    private fun updateView(routePart: RoutePart?) {

        if (routePart == null)
            return

        viewModel.routePart = routePart

        routePart_detail_name.text = viewModel.routePart?.name.toString()
        routePart_detail_description.text = viewModel.routePart?.description.toString()

        if (viewModel.routePart?.challenge == null || viewModel.routePart?.challenge?.quizz == null)
            routePart_detail_quizz_challenge.text = "Keine Beschreibung hinterlegt." // Todo localization
        else
            routePart_detail_quizz_challenge.text = viewModel.routePart?.challenge?.quizz.toString()

        // Update the Map
        mMapView = view?.findViewById(R.id.routePart_detail_mapview)

        // Todo create Queries to retrieve data without using Complex
        if (viewModel.routePart?.cacheToUnlock == null || (viewModel.routePart?.cacheToFinishPart == null))
            viewModel.routePart = viewModel.getRoutePartComplex(routePart.id)

        if (viewModel.routePart?.cacheToUnlock?.coordinates == null)
            viewModel.routePart?.cacheToUnlock = viewModel.getCacheComplex(viewModel.routePart?.cacheToUnlock!!.id)
        if (viewModel.routePart?.cacheToFinishPart?.coordinates == null)
            viewModel.routePart?.cacheToFinishPart = viewModel.getCacheComplex(viewModel.routePart?.cacheToFinishPart!!.id)


        // Check coordinates
        val coordUnlock = GeoPoint(viewModel.routePart?.cacheToUnlock?.coordinates!!.latitude, viewModel.routePart?.cacheToUnlock?.coordinates!!.longitude)
        val coordFinish = GeoPoint(viewModel.routePart?.cacheToFinishPart?.coordinates!!.latitude, viewModel.routePart?.cacheToFinishPart?.coordinates!!.longitude)

        // Show pins on the map
        OSMDroidUtil().placeSimpleMarker(mMapView!!, coordUnlock, "Current: " + viewModel.routePart?.cacheToUnlock!!.name)
        OSMDroidUtil().placeSimpleMarker(mMapView!!, coordFinish, "Next: " + viewModel.routePart?.cacheToFinishPart!!.name)

        var bbox = OSMDroidUtil().getBoundingBox(listOf(coordUnlock, coordFinish))
        mMapView?.addOnFirstLayoutListener(OnFirstLayoutListener { _, _, _, _, _ ->
            // Zoom and scroll to Cache points
            mMapView?.zoomToBoundingBox(bbox, false)

            // Zoom out (only 1 time)
            mMapView?.controller?.zoomOut()

            // recreate map
            mMapView?.invalidate()
        })
    }

}