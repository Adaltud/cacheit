package de.cacheit.view.ui.explore.cache

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.databinding.FragmentCachesBinding
import de.cacheit.model.route.Cache
import de.cacheit.view.ui.core.BaseFragment
import de.cacheit.view.ui.explore.ExploreLandingFragmentDirections
import de.cacheit.view.ui.explore.ExploreViewModel
import kotlinx.android.synthetic.main.fragment_caches.*

@AndroidEntryPoint
class CachesFragment : BaseFragment(R.layout.fragment_caches), CachesAdapter.OnItemClickListener {

    private val viewModel: ExploreViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentCachesBinding.bind(view)
        val cacheAdapter = CachesAdapter(this)

        binding.apply {
            cachesRecyclerView.apply {
                adapter = cacheAdapter
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
            }
        }

        viewModel.caches.observe(viewLifecycleOwner) {
            cacheAdapter.submitList(it)
        }

        caches_fltbtn_new.setOnClickListener {
            val action = ExploreLandingFragmentDirections.actionExploreLandingFragmentToCacheAddEditFragment()
            findNavController().navigate(action)
        }

    }

    override fun onItemClick(cache: Cache) {
        val action = ExploreLandingFragmentDirections.actionExploreLandingFragmentToCacheDetailFragment(cache)
        findNavController().navigate(action)
    }
}