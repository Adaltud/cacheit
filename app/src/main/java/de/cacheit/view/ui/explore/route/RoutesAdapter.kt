package de.cacheit.view.ui.explore.route

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import de.cacheit.databinding.ItemRouteBinding
import de.cacheit.model.route.Route

class RoutesAdapter(private val listener: OnItemClickListener) :
    ListAdapter<Route, RoutesAdapter.RoutesViewHolder>(DiffCallBack()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoutesViewHolder {
        val binding = ItemRouteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RoutesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RoutesViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    public override fun getItem(position: Int): Route {
        return super.getItem(position)
    }

    inner class RoutesViewHolder(private val binding: ItemRouteBinding) :
        RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {

        fun bind(route: Route) {
            binding.apply {
                routeCheckBox.isChecked = false
                routeTextRoutename.text = route.name
            }
        }

        init {
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val route: Route = getItem(adapterPosition)
            listener.onItemClick(route)
        }

    }

    interface OnItemClickListener {
        fun onItemClick(route: Route)
    }

    class DiffCallBack : DiffUtil.ItemCallback<Route>() {

        override fun areItemsTheSame(oldItem: Route, newItem: Route) =
            // Same as" : Boolean { return oldItem.id == newItem.id} "
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Route, newItem: Route) =
            // Same as" : Boolean { return oldItem == newItem} "
            oldItem == newItem

    }
}