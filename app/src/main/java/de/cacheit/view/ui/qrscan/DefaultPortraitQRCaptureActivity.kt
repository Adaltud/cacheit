package de.cacheit.view.ui.qrscan

import android.content.Intent
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.CaptureActivity
import kotlinx.android.synthetic.main.fragment_cache_add_edit.*

/**
 * Capture activity needed for ZXing on order the customize the
 * presented screen when trying to read a QR-Code
 *
 */
class DefaultPortraitQRCaptureActivity : CaptureActivity() {
}