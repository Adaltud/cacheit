package de.cacheit.view.ui.profil

import android.os.Bundle
import android.view.View
import de.cacheit.R
import de.cacheit.view.ui.core.BaseFragment

class ProfilFragment : BaseFragment(R.layout.fragment_profil) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        hideBackButton()
    }

}