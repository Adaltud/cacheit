package de.cacheit.view.ui.explore.route

import android.os.Bundle
import android.view.View
import androidx.core.view.iterator
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.databinding.FragmentRouteAddRoutepartBinding
import de.cacheit.model.route.Route
import de.cacheit.model.route.RoutePart
import de.cacheit.view.ui.core.BaseFragment
import de.cacheit.view.ui.explore.ExploreViewModel
import de.cacheit.view.ui.explore.routePart.RoutePartsAdapter
import kotlinx.android.synthetic.main.fragment_route_add_edit.*
import kotlinx.android.synthetic.main.fragment_route_add_routepart.*
import kotlinx.android.synthetic.main.item_routepart.view.*

@AndroidEntryPoint
class RouteAddRoutePartFragment : BaseFragment(R.layout.fragment_route_add_routepart),
    RoutePartsAdapter.OnItemClickListener {

    private val args: RouteAddRoutePartFragmentArgs by navArgs()
    private val viewModel: ExploreViewModel by viewModels()
    private lateinit var route: Route
    private lateinit var lastRoutePart: RoutePart
    private lateinit var possRouteParts: MutableList<RoutePart>
    private lateinit var selectedRoutePart: RoutePart


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentRouteAddRoutepartBinding.bind(view)
        val routePartAdapter = RoutePartsAdapter(this)

        possRouteParts = mutableListOf()
        lastRoutePart = viewModel.getRouteComplex(args.route.id)?.routeParts!!.last()


        binding.apply {
            routeAddRoutepartRecyclerViewRouteparts.apply {
                adapter = routePartAdapter
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
            }
        }

        viewModel.routeParts.observe(viewLifecycleOwner) {


            val iterator = it.listIterator()
            var tempRoutePart: RoutePart

            while (iterator.hasNext()) {
                tempRoutePart = viewModel.getRoutePartComplex(iterator.next().id)!!
                if (tempRoutePart.cacheToUnlock?.NFCCode == lastRoutePart.cacheToFinishPart?.NFCCode && !(possRouteParts.contains(
                        tempRoutePart
                    ))
                ) {
                    possRouteParts.add(tempRoutePart)
                }
            }
            routePartAdapter.submitList(possRouteParts)
        }

        route_add_routepart_fltbtn_check.setOnClickListener {

            /* find the selected RoutePart, if more then one is selected, the last one will be used,
            TODO change so that just one can be selected, same in cache routepart and Route addEdit files
             */

            route = viewModel.getRouteComplex(args.route.id)!!

            val iterator = route_add_routepart_recyclerView_routeparts.iterator()
            for (i in iterator) {
                if (i.routePart_check_box.isChecked) {
                    selectedRoutePart = routePartAdapter.getItem(
                        route_add_routepart_recyclerView_routeparts.getChildLayoutPosition(i)
                    )
                    route.routeParts?.add(selectedRoutePart)
                    viewModel.updateRoute(route.id, route)
                    break
                }
                if (!iterator.hasNext())
                    break

            }

            val action =
                RouteAddRoutePartFragmentDirections.actionRouteAddRoutePartFragmentToRouteDetailFragment(
                    route
                )
            findNavController().navigate(action)

        }


    }

    override fun onItemClick(routePart: RoutePart) {
        val action =
            RouteAddRoutePartFragmentDirections.actionRouteAddRoutePartFragmentToRoutePartDetailFragment(
                routePart
            )
        findNavController().navigate(action)
    }

}