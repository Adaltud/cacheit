package de.cacheit.view.ui.explore.cache

import android.annotation.SuppressLint
import android.content.Context.LOCATION_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.nfc.NfcAdapter
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.zxing.integration.android.IntentIntegrator
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.model.route.Cache
import de.cacheit.model.route.Coordinates
import de.cacheit.utils.nfc.NFCUtil
import de.cacheit.view.ui.core.BaseFragment
import de.cacheit.view.ui.explore.ExploreViewModel
import de.cacheit.view.ui.qrscan.DefaultPortraitQRCaptureActivity
import kotlinx.android.synthetic.main.fragment_cache_add_edit.*
import java.lang.Exception


@AndroidEntryPoint
class CacheAddEditFragment : BaseFragment(R.layout.fragment_cache_add_edit) {

    private val viewModel: ExploreViewModel by viewModels()
    private val REQUEST_PERMISSIONS_GPS_FILL = 25
    private var nfcAdapter: NfcAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var packetManager = context?.packageManager;

        // Hide NFC button if device does not support it
        if (packetManager?.hasSystemFeature(PackageManager.FEATURE_NFC) == false)
            cache_fltbtn_nfc_code.visibility = View.INVISIBLE

        // Hide QR button if device does not support it
        if (packetManager?.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY) == false)
            cache_fltbtn_qr_code.visibility = View.INVISIBLE

        // Hide GPS button if device does not support it
        if (packetManager?.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS) == false)
            cache_gps_data.visibility = View.INVISIBLE

        cache_fltbtn_check.setOnClickListener {

            if (add_edit_cache_editText_latitude.text.toString().isEmpty())
                add_edit_cache_editText_latitude.setText("0")

            if (add_edit_cache_editText_longitude.text.toString().isEmpty())
                add_edit_cache_editText_longitude.setText("0")

            val name = add_edit_cache_editText_name.text.toString()
            val description = add_edit_cache_editText_description.text.toString()
            val code = add_edit_cache_editText_code.text.toString()
            val longitude = add_edit_cache_editText_longitude.text.toString().toDouble()
            val latitude = add_edit_cache_editText_latitude.text.toString().toDouble()
            val coordinates = Coordinates(latitude, longitude)

            if (name.isEmpty() || description.isEmpty() || code.isEmpty()) {
                Toast.makeText(requireContext(), "Every field needs to be set.", Toast.LENGTH_SHORT).show()
            }
            else {
                val cache = Cache(name, description, code, coordinates)

                if (viewModel.saveCoordinates(coordinates) && viewModel.saveCache(cache)) {
                    val action =
                        CacheAddEditFragmentDirections.actionCacheAddEditFragmentToExploreLandingFragment()
                    findNavController().navigate(action)
                }
            }
        }

        cache_gps_data.setOnClickListener {
            nfcAdapter?.disableReaderMode(activity)

            if (isPermissionGranted(android.Manifest.permission.ACCESS_FINE_LOCATION))
                fetchCoordinates()
            else
                requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_PERMISSIONS_GPS_FILL)
        }

        cache_fltbtn_nfc_code.setOnClickListener {
            if (nfcAdapter == null)
                nfcAdapter = NfcAdapter.getDefaultAdapter(requireContext())

                nfcAdapter?.enableReaderMode(activity,
                {
                    add_edit_cache_editText_code.setText(NFCUtil().getIdString(it))
                    nfcAdapter?.disableReaderMode(activity)
                },
                NfcAdapter.FLAG_READER_NFC_A or NfcAdapter.FLAG_READER_NFC_B or NfcAdapter.FLAG_READER_NFC_F or NfcAdapter.FLAG_READER_NFC_V or NfcAdapter.FLAG_READER_NFC_BARCODE,
                null)
        }

        cache_fltbtn_qr_code.setOnClickListener {
            nfcAdapter?.disableReaderMode(activity)

            val integrator = IntentIntegrator.forSupportFragment(this)
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
            integrator.setBeepEnabled(false)
            integrator.setBarcodeImageEnabled(true)
            integrator.captureActivity = DefaultPortraitQRCaptureActivity::class.java
            integrator.setOrientationLocked(true)
            integrator.initiateScan()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSIONS_GPS_FILL) {
            fetchCoordinates()
        }
    }

    @SuppressLint("MissingPermission") // already handled
    private fun fetchCoordinates() {
        if (isPermissionGranted(android.Manifest.permission.ACCESS_FINE_LOCATION))
        {
            if (locationManager == null)
                locationManager = requireContext().getSystemService(LOCATION_SERVICE) as LocationManager?

            val providers: List<String> = locationManager!!.getProviders(true)
            var location: Location? = null

            for (i in providers.size - 1 downTo 0) {
                location = locationManager?.getLastKnownLocation(providers[i])
                if (location != null)
                    break
            }

            if (location != null && (location.latitude != 0.0 || location.longitude != 0.0))
            {
                add_edit_cache_editText_latitude.setText(location.latitude.toString())
                add_edit_cache_editText_longitude.setText(location.longitude.toString())
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
            if (result != null) {
                add_edit_cache_editText_code.setText(result.contents)
            } else {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
        catch (ex: Exception) {
            Log.e("QR", "Unable to scan QR-Code-Tag.", ex)
        }

    }
}