package de.cacheit.view.ui.explore

import android.os.Bundle
import android.view.View
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.view.ui.core.BaseFragment

@AndroidEntryPoint
class ExploreFragment : BaseFragment(R.layout.fragment_explore) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.hideBackButton()


    }
}