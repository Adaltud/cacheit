package de.cacheit.view.ui.maps

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.view.ui.core.BaseFragment
import org.osmdroid.config.Configuration
import org.osmdroid.events.DelayedMapListener
import org.osmdroid.events.MapListener
import org.osmdroid.events.ScrollEvent
import org.osmdroid.events.ZoomEvent
import org.osmdroid.tileprovider.MapTileProviderBasic
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.Overlay
import org.osmdroid.views.overlay.compass.CompassOverlay
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay
import java.lang.Exception

@AndroidEntryPoint
class MapsFragment : BaseFragment(R.layout.fragment_maps){

    private var mMapView: MapView? = null
    //private val viewModel by viewModels<MapsViewModel>()
    private val viewModel: MapsViewModel by viewModels()
    private val loadedMarkers: MutableMap<Long, Overlay> = mutableMapOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        this.hideBackButton()

        // Create Mapview
        mMapView = MapView(requireContext(), MapTileProviderBasic(requireContext(), TileSourceFactory.MAPNIK))
        mMapView?.controller?.setZoom(16.0)
        mMapView?.addMapListener(DelayedMapListener(object : MapListener {
            override fun onZoom(e: ZoomEvent) : Boolean {
                getPositionRouteOverlays()
                return true;
            }

            override fun onScroll(e: ScrollEvent) : Boolean {
                getPositionRouteOverlays()
                return true;
            }
        }, 1000 ));

        createDefaultMapOverlays()


        val root: View = inflater.inflate(R.layout.fragment_maps, container, false)
        (root.findViewById<View>(R.id.mapviewfragment) as LinearLayout).addView(mMapView)

        // Request permissions for OSMDroid
        requestPermissionsIfNecessary(arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.ACCESS_FINE_LOCATION
        ));
        return root
    }

    override fun onResume() {
        super.onResume()
        mMapView!!.onResume()
    }

    override fun onPause() {
        super.onPause()
        mMapView!!.onPause()
    }

    private fun createDefaultMapOverlays() {

        // get provider for GPS data
        val provider = GpsMyLocationProvider(requireContext())
        provider.addLocationSource(LocationManager.GPS_PROVIDER);
        provider.locationUpdateMinTime = 100

        // create locationoverlay
        val overlay = MyLocationNewOverlay(provider, mMapView)
        overlay.enableMyLocation()
        overlay.enableFollowLocation()

        // create compass overlay
        val compas = CompassOverlay(requireContext(), InternalCompassOrientationProvider(requireContext()), mMapView)
        compas.isEnabled = true
        compas.enableCompass()

        // add overlays to map
        mMapView?.overlayManager?.addAll(arrayListOf( overlay, compas))
    }

    private fun getPositionRouteOverlays() {
        try {
            var vm = viewModel
            var ls = vm.getRoutesInBound(mMapView!!.boundingBox!!.latSouth, mMapView!!.boundingBox.latNorth, mMapView!!.boundingBox.lonWest, mMapView!!.boundingBox.lonEast)

            ls.forEach { route ->
                run {
                    if (!loadedMarkers.contains(route.id)) {
                        val routepart = viewModel.getStartRoutePartComplex(route.id)
                        val coord = viewModel.getCoordinatesById(routepart!!.id)
                        val m = Marker(mMapView)
                        m.position = GeoPoint(coord!!.latitude, coord.longitude)
                        m.textLabelBackgroundColor = Color.TRANSPARENT
                        m.textLabelForegroundColor = Color.RED
                        m.textLabelFontSize = 40
                        m.icon = getResources().getDrawable(R.drawable.ic_map_marker_medium);
                        m.title = route.name
                        m.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_TOP)

                        mMapView?.overlayManager?.add(m);
                        loadedMarkers[route.id] = m
                    }
                }
            }

            // Remove any marker which isn't visible anymore
            var removedIds = mutableListOf<Long>()

            loadedMarkers.forEach { marker ->
                if (!ls.any { x -> x.id == marker.key}) {
                    mMapView?.overlayManager?.remove(marker.value)
                    removedIds.add(marker.key)
                }
            }

            // Remove all removed marker ids which are not loaded anymore
            removedIds.forEach { x -> loadedMarkers.remove(x) }
        }
        catch (ex: Exception) {

        }


    }
}