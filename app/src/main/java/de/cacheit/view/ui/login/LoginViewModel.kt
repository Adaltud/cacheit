package de.cacheit.view.ui.login

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import de.cacheit.model.Person
import de.cacheit.storage.repositories.PersonRepository
import kotlinx.coroutines.runBlocking

class LoginViewModel @ViewModelInject constructor(
    private val personRepo: PersonRepository,

) : ViewModel() {

    val persons = runBlocking { personRepo.getAllLiveAwait() }

    fun checkLogin(email:String,password:String) : Person? {
       return runBlocking {  personRepo.checkLogin(email,password)}
    }

    fun savePerson(person: Person):Boolean {
        val id: Long
        runBlocking { id = personRepo.insertComplexAutoAwait(person) }
        return id != 0L
    }

    fun getPersonComplex(id: Long): Person? {
        return runBlocking { personRepo.getByIdComplexAutoAwait(id) }

    }
}