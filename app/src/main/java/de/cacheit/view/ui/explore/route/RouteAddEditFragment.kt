package de.cacheit.view.ui.explore.route

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.iterator
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.databinding.FragmentRouteAddEditBinding
import de.cacheit.model.route.Route
import de.cacheit.model.route.RoutePart
import de.cacheit.view.ui.core.BaseFragment
import de.cacheit.view.ui.explore.ExploreViewModel
import de.cacheit.view.ui.explore.cache.CachesAdapter
import de.cacheit.view.ui.explore.routePart.RoutePartsAdapter
import kotlinx.android.synthetic.main.fragment_route_add_edit.*
import kotlinx.android.synthetic.main.item_routepart.view.*

@AndroidEntryPoint
class RouteAddEditFragment : BaseFragment(R.layout.fragment_route_add_edit), RoutePartsAdapter.OnItemClickListener {

    private val viewModel: ExploreViewModel by viewModels()
    private lateinit var startRoutePart: RoutePart
    private var startRoutePartList: MutableList<RoutePart> = mutableListOf()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentRouteAddEditBinding.bind(view)
        val routePartAdapter = RoutePartsAdapter(this)

        // Binds the RecyclerView for the RouteParts
        binding.apply {
            addEditRouteRecyclerViewRouteparts.apply {
                adapter = routePartAdapter
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
            }
        }

        // Observes the RouteParts, which the Viewmodel gets as LiveData
        viewModel.routeParts.observe(viewLifecycleOwner){
            routePartAdapter.submitList(it)
        }

        route_fltbtn_check.setOnClickListener {

            val name = add_edit_route_route_name.text.toString()
            val description = add_edit_route_route_description.text.toString()


                startRoutePart = RoutePart()
                for (i in add_edit_route_recyclerView_routeparts.iterator()) {
                    if (i.routePart_check_box.isChecked) {
                        startRoutePart = routePartAdapter.getItem(
                            add_edit_route_recyclerView_routeparts.getChildLayoutPosition(i)
                        )
                        break
                    }
                }

            if (name.isEmpty() || description.isEmpty() || startRoutePart.id == 0L) {
                Toast.makeText(requireContext(), "Every field needs to be set.", Toast.LENGTH_SHORT).show()
            }
            else {

                startRoutePartList.add(startRoutePart)

                val route = Route(name, description, 0, 0, 0, startRoutePart, startRoutePartList)

                if (viewModel.saveRoute(route)) {
                    val action =
                        RouteAddEditFragmentDirections.actionRouteAddEditFragmentToExploreLandingFragment()
                    findNavController().navigate(action)
                }
            }
        }

    }

    override fun onItemClick(routePart: RoutePart) {
        val action = RouteAddEditFragmentDirections.actionRouteAddEditFragmentToRoutePartDetailFragment(routePart)
        findNavController().navigate(action)
    }
}