package de.cacheit.view.ui.login

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import de.cacheit.R
import de.cacheit.view.ui.core.BaseFragment
import kotlinx.android.synthetic.main.fragment_login_landing.*

@AndroidEntryPoint
class LoginLandingFragment : BaseFragment(R.layout.fragment_login_landing) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        landingButtonSignIn.setOnClickListener {
            val action = LoginLandingFragmentDirections.actionLandingFragmentToSignInFragment()
            findNavController().navigate(action)
        }

        landingButtonSignUp.setOnClickListener {
            val action = LoginLandingFragmentDirections.actionLandingFragmentToSignUpFragment()
            findNavController().navigate(action)
        }

    }




}