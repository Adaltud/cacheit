package de.cacheit.storage


import de.cacheit.model.BaseModel
import retrofit2.Response

/**
 * Interface to define external methods in order to resolve data,
 * if the default data store does not contain a specific record.
 *
 * @param T Type of the elements to resolve.
 */
@FunctionalInterface
interface IStorageResolver<T> {
    suspend fun getById(id: Long) : T?
    // Currently we only need the 'getById' method, but future releases could add methods like
    // suspend fun getAll() : List<T>?
}