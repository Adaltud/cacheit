package de.cacheit.storage.repositories.route

import de.cacheit.model.route.RoutePart
import de.cacheit.storage.local.dao.route.RoutePartDao
import de.cacheit.storage.remote.model.route.RRoutePart
import de.cacheit.storage.remote.repositories.route.RRoutePartRepository
import de.cacheit.storage.repositories.BaseRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RoutePartRepository @Inject constructor(dao: RoutePartDao, remoteRepo: RRoutePartRepository) : BaseRepository<RoutePartDao, RoutePart, RRoutePart>(dao, remoteRepo)  {
}