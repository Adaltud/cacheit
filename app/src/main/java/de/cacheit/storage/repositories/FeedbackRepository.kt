package de.cacheit.storage.repositories

import android.app.Application
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import de.cacheit.model.Feedback
import de.cacheit.model.Person
import de.cacheit.storage.local.CacheITDatabase
import de.cacheit.storage.local.dao.FeedbackDao
import de.cacheit.storage.local.dao.PersonDao
import de.cacheit.storage.remote.model.RFeedback
import de.cacheit.storage.remote.repositories.RFeedbackRepository
import de.cacheit.storage.remote.repositories.RPersonRepository
import java.util.concurrent.Callable
import java.util.stream.Collectors
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FeedbackRepository @Inject constructor(dao: FeedbackDao, remoteRepo: RFeedbackRepository) : BaseRepository<FeedbackDao, Feedback, RFeedback>(dao, remoteRepo)  {
}