package de.cacheit.storage.repositories


import de.cacheit.model.Person
import de.cacheit.storage.local.dao.PersonDao
import de.cacheit.storage.remote.model.RPerson
import de.cacheit.storage.remote.repositories.RPersonRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PersonRepository @Inject constructor(dao: PersonDao, remoteRepo: RPersonRepository) :
    BaseRepository<PersonDao, Person, RPerson>(dao, remoteRepo) {
    suspend fun checkLogin(email: String, password: String) =
        (dao as PersonDao).checkLogin(email, password)
}