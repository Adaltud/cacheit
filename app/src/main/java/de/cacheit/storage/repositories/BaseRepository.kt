package de.cacheit.storage.repositories

import androidx.lifecycle.LiveData
import de.cacheit.core.extensions.emptyToNull
import de.cacheit.model.BaseModel
import de.cacheit.storage.IStorageResolver
import de.cacheit.storage.LocalRemoteMapper
import de.cacheit.storage.local.dao.BaseDao
import de.cacheit.storage.remote.model.RBaseModel
import de.cacheit.storage.remote.repositories.RBaseRepository
import de.cacheit.utils.Contexts
import de.cacheit.utils.network.InternetUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

/**
 * Base class for all repositories using the local data store.
 *
 * @param T DAO class to use for the entity/model K.
 * @param K Local entity/model class.
 * @param I Remote entity/model class, used for retrieving data from the
 * remote data store if the record does not exist in the local data store.
 * @property dao Instance of the DAO class T
 * @property remoteRepo Instance  of the remote repository class.
 */
abstract class BaseRepository<T, K, I : RBaseModel<I>> constructor(val dao : BaseDao<K>, val remoteRepo : RBaseRepository<I>)
        where T : BaseDao<K>, K : BaseModel
{
    // No need to store LiveData observer in local variable, since repositories are Singletons

    // Our storage concept "local first, remote later", so check local data store for entity, if it doesn't exist, check online
    // Insert, Update and Delete actions will be pushed in intervals

    @Inject
    lateinit var mapper: LocalRemoteMapper

    private val remoteResolver = object : IStorageResolver<K> {
        override suspend fun getById(id: Long): K? = map2Local(remoteRepo.getById(id))
    }


    init {
        dao.addResolver(remoteResolver)
    }

    /**
     * Check if a record with the given id exists.
     *
     * @param id The id to check.
     * @return True if the record exist, otherwise false.
     */
    fun exists(id: Long): Boolean = dao.exists(id)


    // region Get LiveData

    fun getByIdLive(id: Long): LiveData<K> =
        dao.getByIdLive(id).emptyToNull() ?: runBlocking { fetchAndInsertRemoteByIdAwait(id); dao.getByIdLive(id) }

    fun getAllLive(forceRemote: Boolean = false): LiveData<List<K>> =
        if (forceRemote) runBlocking { fetchAndInsertOrUpdateRemoteAllAwait(); dao.getAllLive() }
        else dao.getAllLive().emptyToNull() ?: runBlocking { fetchAndInsertOrUpdateRemoteAllAwait(); dao.getAllLive() }

    fun getLiveDataListByIds(ids: Array<Long>, forceRemote: Boolean = false): LiveData<List<K>> =
        if (forceRemote) runBlocking { fetchAndInsertOrUpdateRemoteByIdsAwait(ids.toList()); dao.getLiveDataListByIds(ids) }
        else dao.getLiveDataListByIds(ids).emptyToNull() ?: runBlocking { fetchAndInsertOrUpdateRemoteByIdsIfNecessary(ids.toList()); dao.getLiveDataListByIds(ids) }

    suspend fun getByIdLiveAwait(id: Long): LiveData<K> =
        withContext(Dispatchers.Default) { dao.getByIdLive(id).emptyToNull() ?: fetchAndInsertRemoteByIdAwait(id); dao.getByIdLive(id) }

    suspend fun getAllLiveAwait(forceRemote: Boolean = false): LiveData<List<K>> =
        if (forceRemote) withContext(Dispatchers.Default) { fetchAndInsertOrUpdateRemoteAllAwait(); dao.getAllLive() }
        else withContext(Dispatchers.Default) { dao.getAllLive().emptyToNull() ?: fetchAndInsertOrUpdateRemoteAllAwait(); dao.getAllLive() }

    suspend fun getLiveDataListByIdsAwait(ids: Array<Long>, forceRemote: Boolean = false): LiveData<List<K>> =
        if (forceRemote) withContext(Dispatchers.Default)  { fetchAndInsertOrUpdateRemoteByIdsAwait(ids.toList()); dao.getLiveDataListByIds(ids) }
        else withContext(Dispatchers.Default) { dao.getLiveDataListByIds(ids).emptyToNull() ?: fetchAndInsertOrUpdateRemoteByIdsIfNecessary(ids.toList()); dao.getLiveDataListByIds(ids)  }

    // endregion Get LiveData
    
    // region Get "normal" data

    fun getById(id: Long): K? =
        dao.getById(id) ?: runBlocking { fetchAndInsertRemoteByIdAwait(id); dao.getById(id) }

    fun getByIds(ids: Array<Long>, forceRemote: Boolean = false): List<K> =
        if (forceRemote) runBlocking { fetchAndInsertOrUpdateRemoteByIdsAwait(ids.toList()); dao.getByIds(ids) }
        else dao.getByIds(ids).emptyToNull() ?: runBlocking { fetchAndInsertOrUpdateRemoteByIdsIfNecessary(ids.toList()); dao.getByIds(ids) }

    fun getAll(forceRemote: Boolean = false): List<K> =
        if (forceRemote) runBlocking { fetchAndInsertOrUpdateRemoteAllAwait(); dao.getAll() }
        else dao.getAll().emptyToNull() ?: runBlocking { fetchAndInsertOrUpdateRemoteAllAwait(); dao.getAll() }


    suspend fun getByIdAwait(id: Long): K? =
        dao.getByIdAwait(id) ?: dao.getByIdAwait(fetchAndInsertRemoteByIdAwait(id))

    suspend fun getByIdsAwait(ids: Array<Long>, forceRemote: Boolean = false): List<K> =
        if (forceRemote) withContext(Dispatchers.Default) { fetchAndInsertOrUpdateRemoteByIdsAwait(ids.toList()); dao.getByIds(ids) }
        else dao.getByIds(ids).emptyToNull() ?: withContext(Dispatchers.Default) { fetchAndInsertOrUpdateRemoteByIdsIfNecessary(ids.toList()); dao.getByIds(ids) }


    suspend fun getAllAwait(forceRemote: Boolean = false): List<K> =
        if (forceRemote) { fetchAndInsertOrUpdateRemoteAllAwait(); dao.getAllAwait() }
        else dao.getAllAwait().emptyToNull() ?: withContext(Dispatchers.Default) { fetchAndInsertOrUpdateRemoteAllAwait(); dao.getAllAwait() }

    //endregion Get "normal" data

    // region Get Complex data

    fun getByIdComplexAuto(id: Long, vararg fieldNames: String ) =
        dao.getByIdComplexAuto(id, fieldNames.asList()) ?: runBlocking { fetchAndInsertRemoteByIdAwait(id); dao.getByIdComplexAuto(id, fieldNames.asList()) }

    fun getByIdComplexAuto(id: Long, fieldNames: List<String> ) =
        dao.getByIdComplexAuto(id, fieldNames) ?: runBlocking { fetchAndInsertRemoteByIdAwait(id); dao.getByIdComplexAuto(id, fieldNames) }

    fun getByIdComplexAuto(id: Long) =
        dao.getByIdComplexAuto(id) ?: runBlocking { fetchAndInsertRemoteByIdAwait(id); dao.getByIdComplexAuto(id) }


    suspend fun getByIdComplexAutoAwait(id: Long, vararg fieldNames: String ) = dao.getByIdComplexAutoAwait(id, fieldNames.asList())
    suspend fun getByIdComplexAutoAwait(id: Long, fieldNames: List<String> ) = dao.getByIdComplexAutoAwait(id, fieldNames)
    suspend fun getByIdComplexAutoAwait(id: Long) = dao.getByIdComplexAutoAwait(id)

    //endregion Get Complex data


    // region INSERT, UPDATE and DELETE
    // Insert
    fun insert(entity: K): Long  = dao.insert(entity)
    fun insert(entities: List<K>): List<Long> = dao.insert(entities)
    fun insert(vararg entities: K): List<Long> = dao.insert(*entities)
    suspend fun insertAwait(entity: K): Long  = dao.insertAwait(entity)
    suspend fun insertAwait(entities: List<K>): List<Long> = dao.insertAwait(entities)
    suspend fun insertAwait(vararg entities: K): List<Long> = dao.insertAwait(*entities)

    // Insert complex
    fun insertComplexAuto(entity: K): Long  = dao.insertComplexAuto(entity)
    fun insertComplexAuto(entities: List<K>): List<Long> = dao.insertComplexAuto(entities)
    fun insertComplexAuto(vararg entities: K): List<Long> = dao.insertComplexAuto(*entities)
    suspend fun insertComplexAutoAwait(entity: K): Long  = dao.insertComplexAutoAwait(entity)
    suspend fun insertComplexAutoAwait(entities: List<K>): List<Long> = dao.insertComplexAutoAwait(entities)
    suspend fun insertComplexAutoAwait(vararg entities: K): List<Long> = dao.insertComplexAutoAwait(*entities)

    // Update
    fun update(entity: K) = dao.update(entity)
    fun update(entities: List<K>) = dao.update(entities)
    fun update(vararg entities: K) = dao.update(*entities)
    suspend fun updateAwait(entity: K) = dao.updateAwait(entity)
    suspend fun updateAwait(entities: List<K>) = dao.updateAwait(entities)
    suspend fun updateAwait(vararg entities: K) = dao.updateAwait(*entities)

    // Update complex
    fun updateComplexAuto(entity: K) = dao.updateComplexAuto(entity)
    fun updateComplexAuto(entities: List<K>) = dao.updateComplexAuto(entities)
    fun updateComplexAuto(vararg entities: K) = dao.updateComplexAuto(*entities)
    suspend fun updateComplexAutoAwait(entity: K) = dao.updateComplexAutoAwait(entity)
    suspend fun updateComplexAutoAwait(entities: List<K>) = dao.updateComplexAutoAwait(entities)
    suspend fun updateComplexAutoAwait(vararg entities: K) = dao.updateComplexAutoAwait(*entities)

    // Delete
    fun delete(entity: K) = dao.delete(entity)
    fun delete(entities: List<K>) = dao.delete(entities)
    fun delete(vararg entities: K) = dao.delete(*entities)
    fun deleteAll() = dao.deleteAll()
    suspend fun deleteAwait(entity: K) = dao.deleteAwait(entity)
    suspend fun deleteAwait(entities: List<K>) = dao.deleteAwait(entities)
    suspend fun deleteAwait(vararg entities: K) = dao.deleteAwait(*entities)
    suspend fun deleteAllAwait() = dao.deleteAllAwait()

    // endregion

    /**
     * Get a remote entity/model specified by the given id, convert it to a local one and
     * insert it to the local data store.
     *
     * @param ids The ids of the records to fetch.
     * @return The id of the inserted record or 0 if the record hasn't been found.
     */
    private suspend fun fetchAndInsertOrUpdateRemoteByIdsIfNecessary(ids: List<Long>, forceRemote: Boolean = false): List<Long> {
        if (isInternetAvailable()) {
            var remoteIdsToFetch = mutableListOf<Long>()

            if (forceRemote)
                remoteIdsToFetch = ids.toMutableList()
            else
                ids.forEach { id ->
                    if (!exists(id))
                        remoteIdsToFetch.add(id)
                }

            fetchAndInsertOrUpdateRemoteByIdsAwait(remoteIdsToFetch)
        }
        return ids
    }


    /**
     * Get all remote entities/models specified by the given ids, convert them to local ones and
     * insert them to the local data store.
     *
     * @param ids Record ids to fetch.
     * @return
     */
    private suspend fun fetchAndInsertOrUpdateRemoteByIdsAwait(ids: List<Long>): List<Long> {
        val result = mutableListOf<Long>()
        if (isInternetAvailable()) {
            // remote API in v1 does not contains a function to fetch multiple record by given ids in a single request
            // so we need to do a request for every id... definitely performance potential here...
            ids.forEach { id ->
                val created = map2Local(remoteRepo.getById(id))
                if (created != null) {
                    if (exists(id))
                        updateComplexAutoAwait(created)
                    else
                        insertComplexAutoAwait(created)

                    result.add(created.id)
                }
            }
        }
        return result
    }


    /**
     * Get a remote entity/model specified by the given id, convert it to a local one and
     * insert it to the local data store.
     *
     * @param id The id of the record to fetch.
     * @return The id of the inserted record or 0 if the record hasn't been found.
     */
    private suspend fun fetchAndInsertRemoteByIdAwait(id: Long): Long {
        if (isInternetAvailable()) {
            val result = map2Local(remoteRepo.getById(id))
            if (result != null)
                return insertComplexAutoAwait(result)
        }
        return 0
    }

    /**
     * Get all remote entities/models, convert them to local ones and
     * insert them to the local data store.
     *
     * @return The ids of the inserted/updated records or an empty list if no record has been found.
     */
    private suspend fun fetchAndInsertOrUpdateRemoteAllAwait(): List<Long> {
        val result = mutableListOf<Long>()
        if (isInternetAvailable()) {
            map2Local(remoteRepo.getAll())?.forEach { record ->
                if (exists(record.id))
                    updateComplexAutoAwait(record)
                else
                    insertComplexAutoAwait(record)

                result.add(record.id)
            }
        }
        return result
    }

    /**
     * Get a specific remote record and insert it in the local data store.
     *
     * @param id The id of the record to fetch.
     * @return The local id.
     */
    private suspend fun fetchAndInsertRemoteByIdComplexAwait(id: Long): Long {
        if (isInternetAvailable()) {
            val result = map2Local(remoteRepo.getById(id))
            if (result != null)
                return insertComplexAutoAwait(result)
        }
        return 0
    }

    /**
     * Converts a remote entity/model to a local one.
     *
     * @param entity The entity/model to convert.
     * @return Returns the converted entity/model or null.
     */
    private fun map2Local(entity : I?) : K? {
        if (entity == null) return null
        return mapper.map(entity, dao.modelClazz)
    }

    /**
     * Converts all passed remote entities/models to local ones.
     *
     * @param entities The entities/models to convert.
     * @return Returns the converted entities/models or an empty list.
     */
    private fun map2Local(entities : List<I>?) : List<K> {
        if (entities == null) return listOf()
        return mapper.map(entities, dao.modelClazz)
    }

    // Todo create backgound listener and don't instantiate this on every data request
    fun isInternetAvailable() = InternetUtils().isInternetAvailable(Contexts.get())
}
