package de.cacheit.storage.repositories.challenge

import android.content.Context
import androidx.arch.core.util.Function
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import de.cacheit.model.challenge.TextChallenge
import de.cacheit.storage.local.CacheITDatabase
import de.cacheit.storage.local.dao.PersonDao
import de.cacheit.storage.local.dao.challenge.TextChallengeDao
import de.cacheit.storage.remote.model.challenge.RTextRChallenge
import de.cacheit.storage.remote.repositories.RPersonRepository
import de.cacheit.storage.remote.repositories.challenge.RTextChallengeRepository
import de.cacheit.storage.repositories.BaseRepository
import java.util.concurrent.Callable
import java.util.stream.Collectors
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TextChallengeRepository @Inject constructor(dao: TextChallengeDao, remoteRepo: RTextChallengeRepository) :
    BaseRepository<TextChallengeDao, TextChallenge, RTextRChallenge>(dao, remoteRepo)  {
}