package de.cacheit.storage.repositories.route

import android.app.Application
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import dagger.Provides
import de.cacheit.model.Feedback
import de.cacheit.model.Person
import de.cacheit.model.route.Cache
import de.cacheit.model.route.Route
import de.cacheit.model.route.RoutePart
import de.cacheit.storage.local.CacheITDatabase
import de.cacheit.storage.local.dao.FeedbackDao
import de.cacheit.storage.local.dao.PersonDao
import de.cacheit.storage.local.dao.route.RouteDao
import de.cacheit.storage.remote.model.route.RRoute
import de.cacheit.storage.remote.repositories.RPersonRepository
import de.cacheit.storage.remote.repositories.route.RRoutePartRepository
import de.cacheit.storage.remote.repositories.route.RRouteRepository
import de.cacheit.storage.repositories.BaseRepository
import java.util.concurrent.Callable
import java.util.stream.Collectors
import javax.inject.Inject
import javax.inject.Singleton

class RouteRepository @Inject constructor(dao: RouteDao, remoteRepo: RRouteRepository) : BaseRepository<RouteDao, Route, RRoute>(dao, remoteRepo)  {
    suspend fun getRoutesInBoundAwait(latitudeLowerBound: Double, latitudeUpperBound: Double,
                         longitudeLowerBound: Double, longitudeUpperBound: Double, limit: Int) : List<Route>
        = (dao as RouteDao).getRoutesInBoundAwait(latitudeLowerBound, latitudeUpperBound, longitudeLowerBound, longitudeUpperBound, limit)

    suspend fun getStartRoutePartIdAwait(id: Long): Long = (dao as RouteDao).getStartRoutePartIdAwait(id)

}