package de.cacheit.storage.repositories.route

import android.app.Application
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import de.cacheit.model.Feedback
import de.cacheit.model.Person
import de.cacheit.model.route.Cache
import de.cacheit.model.route.Coordinates
import de.cacheit.model.route.Route
import de.cacheit.storage.local.CacheITDatabase
import de.cacheit.storage.local.dao.FeedbackDao
import de.cacheit.storage.local.dao.PersonDao
import de.cacheit.storage.local.dao.route.CacheDao
import de.cacheit.storage.local.dao.route.CoordinatesDao
import de.cacheit.storage.local.dao.route.RouteDao
import de.cacheit.storage.remote.model.route.RCoordinates
import de.cacheit.storage.remote.repositories.RPersonRepository
import de.cacheit.storage.remote.repositories.route.RCoordinatesRepository
import de.cacheit.storage.repositories.BaseRepository
import java.util.concurrent.Callable
import java.util.stream.Collectors
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CoordinatesRepository @Inject constructor(dao: CoordinatesDao, remoteRepo: RCoordinatesRepository) : BaseRepository<CoordinatesDao, Coordinates, RCoordinates>(dao, remoteRepo)  {
}