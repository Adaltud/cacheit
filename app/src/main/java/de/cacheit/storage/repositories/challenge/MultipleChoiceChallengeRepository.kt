package de.cacheit.storage.repositories.challenge

import android.app.Application
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.room.Query
import de.cacheit.model.Feedback
import de.cacheit.model.Person
import de.cacheit.model.challenge.MultipleChoiceChallenge
import de.cacheit.model.challenge.TextChallenge
import de.cacheit.model.route.Cache
import de.cacheit.model.route.Route
import de.cacheit.storage.local.CacheITDatabase
import de.cacheit.storage.local.dao.FeedbackDao
import de.cacheit.storage.local.dao.PersonDao
import de.cacheit.storage.local.dao.challenge.MultipleChoiceChallengeDao
import de.cacheit.storage.local.dao.challenge.TextChallengeDao
import de.cacheit.storage.local.dao.route.CacheDao
import de.cacheit.storage.local.dao.route.RouteDao
import de.cacheit.storage.remote.model.challenge.RMultipleChoiceRChallenge
import de.cacheit.storage.remote.repositories.RPersonRepository
import de.cacheit.storage.remote.repositories.challenge.RMultipleChoiceChallengeRepository
import de.cacheit.storage.repositories.BaseRepository
import java.util.concurrent.Callable
import java.util.stream.Collectors
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MultipleChoiceChallengeRepository @Inject constructor(dao: MultipleChoiceChallengeDao, remoteRepo: RMultipleChoiceChallengeRepository)
    : BaseRepository<MultipleChoiceChallengeDao, MultipleChoiceChallenge, RMultipleChoiceRChallenge>(dao, remoteRepo)  {
}