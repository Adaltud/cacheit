package de.cacheit.storage.repositories.route

import androidx.lifecycle.LiveData
import de.cacheit.model.route.Cache
import de.cacheit.storage.local.dao.route.CacheDao
import de.cacheit.storage.remote.model.route.RCache
import de.cacheit.storage.remote.repositories.RPersonRepository
import de.cacheit.storage.remote.repositories.route.RCacheRepository
import de.cacheit.storage.repositories.BaseRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CacheRepository @Inject constructor(dao: CacheDao, remoteRepo: RCacheRepository) : BaseRepository<CacheDao, Cache, RCache>(dao, remoteRepo)  {
    //var personDao: CacheDao? = null
    val allCache: LiveData<List<Cache>>? = null

    //suspend fun postCache(post)

    data class CacheResponse(
        val coordinates: Coordinates,
        val created: String,
        val description: String,
        val id: Int,
        val modified: Any,
        val name: String,
        val nfccode: Any
    )
    data class Coordinates(
        val latitude: Double,
        val longitude: Double
    )

}