package de.cacheit.storage.remote.repositories

import android.util.Log
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import de.cacheit.storage.remote.CacheITAPI
import de.cacheit.storage.remote.model.RBaseModel
import de.cacheit.storage.remote.model.RPerson
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.lang.Exception

abstract class RBaseRepository<T : RBaseModel<T>> {

    // Todo move somewhere global
    private val BASE_URL = "http://192.168.178.55:8080/api/v1/" //178.55 = TIMS PC

    // Create Moshi and Retrofit instance
    val moshi by lazy{
        Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    }

    protected val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
            .build()
            .create(CacheITAPI::class.java)
    }


    suspend inline fun post(entity: T) : T? {
        // Convert object to JSON string
        val jsonObjectString = entity.toJSON()

        // Create RequestBody
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        try {
            // Send POST
            val response = createPOST(requestBody)

            // Get response and convert to object
            return entity.fromJSON(response.body()!!.string())

        } catch (ex: Exception) {
            return null
        }
    }

    suspend fun getAll() : List<T>? {
        val response = getAllWithResponse()
        return response.body()
    }

    abstract suspend fun createPOST(requestBody: RequestBody) : Response<ResponseBody>
    abstract suspend fun getAllWithResponse() : Response<List<T>>
    abstract suspend fun getById(id: Long) : T?
}