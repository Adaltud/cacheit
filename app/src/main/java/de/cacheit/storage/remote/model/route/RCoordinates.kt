package de.cacheit.storage.remote.model.route

import android.os.Parcelable
import androidx.room.ColumnInfo
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import de.cacheit.storage.remote.model.RBaseModel
import kotlinx.android.parcel.Parcelize


/**
 * RModels have the same fiels as normal models, so the documentation can be found in the original models under:
 * src --> main --> java --> de.cachit --> model
 *
 */
@Parcelize
data class RCoordinates (
    @ColumnInfo(name = "latitude")
    var latitude: Double = 0.0,

    @ColumnInfo(name = "longitude")
    var longitude: Double = 0.0
) : RBaseModel<RCoordinates>(), Parcelable {

    /**
     * Convert an object to a JSON string
     *
     * @return the object serialized as JSON
     */
    override fun toJSON(): String {
        return Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(RCoordinates::class.java).toJson(
            this)
    }

    /**
     * Convert a JSON string to an instance.
     *
     * @param json The JSON string to deserialize.
     * @return The deserialized instance.
     */
    override fun fromJSON(json: String): RCoordinates? {
        return Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(RCoordinates::class.java).lenient().fromJson(json)
    }
}
