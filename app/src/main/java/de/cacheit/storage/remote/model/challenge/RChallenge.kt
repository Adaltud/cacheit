package de.cacheit.storage.remote.model.challenge

import android.os.Parcelable
import de.cacheit.storage.remote.model.RBaseModel

/**
 * RModels have the same fiels as normal models, so the documentation can be found in the original models under:
 * src --> main --> java --> de.cachit --> model
 *
 */
abstract class RChallenge<T> (

    var quizz: String = "",

    var correctAnswer: String = ""
) : RBaseModel<T>(), Parcelable
