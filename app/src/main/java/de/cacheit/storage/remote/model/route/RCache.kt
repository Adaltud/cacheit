package de.cacheit.storage.remote.model.route

import android.os.Parcelable
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

import de.cacheit.storage.remote.model.RBaseModel
import kotlinx.android.parcel.Parcelize


/**
 * RModels have the same fiels as normal models, so the documentation can be found in the original models under:
 * src --> main --> java --> de.cachit --> model
 *
 */
@Parcelize
data class RCache (

    var name: String = "",

    var description: String = "",

    var NFCCode: String = "",

    var coordinates: RCoordinates? = null

) : RBaseModel<RCache>(), Parcelable {

    /**
     * Convert an object to a JSON string
     *
     * @return the object serialized as JSON
     */
    override fun toJSON(): String {
        return Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(RCache::class.java).toJson(
            this)
    }

    /**
     * Convert a JSON string to an instance.
     *
     * @param json The JSON string to deserialize.
     * @return The deserialized instance.
     */
    override fun fromJSON(json: String): RCache? {
        return Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(RCache::class.java).lenient().fromJson(json)
    }
}

