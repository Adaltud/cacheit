package de.cacheit.storage.remote.repositories.route

import de.cacheit.storage.remote.model.RFeedback
import de.cacheit.storage.remote.model.route.RCoordinates
import de.cacheit.storage.remote.repositories.RBaseRepository
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RCoordinatesRepository @Inject constructor(): RBaseRepository<RCoordinates>() {
    override suspend fun createPOST(requestBody: RequestBody): Response<ResponseBody> = retrofit.createCoordinates(requestBody)
    override suspend fun getAllWithResponse(): Response<List<RCoordinates>> = retrofit.getAllCoordinates()
    override suspend fun getById(id: Long): RCoordinates? = retrofit.getCoordinates(id).body()
}