package de.cacheit.storage.remote.repositories.route

import de.cacheit.storage.remote.model.route.RRoutePart
import de.cacheit.storage.remote.repositories.RBaseRepository
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RRoutePartRepository @Inject constructor() : RBaseRepository<RRoutePart>() {
    override suspend fun createPOST(requestBody: RequestBody): Response<ResponseBody> = retrofit.createRouteParts(requestBody)
    override suspend fun getAllWithResponse(): Response<List<RRoutePart>> = retrofit.getAllRouteParts()
    override suspend fun getById(id: Long): RRoutePart? = retrofit.getRoutePart(id).body()
}