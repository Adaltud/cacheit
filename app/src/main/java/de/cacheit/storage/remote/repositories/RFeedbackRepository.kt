package de.cacheit.storage.remote.repositories

import de.cacheit.storage.remote.model.RFeedback
import de.cacheit.storage.remote.model.RPerson
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RFeedbackRepository @Inject constructor() : RBaseRepository<RFeedback>() {
    override suspend fun createPOST(requestBody: RequestBody): Response<ResponseBody> = retrofit.createFeedback(requestBody)
    override suspend fun getAllWithResponse(): Response<List<RFeedback>> = retrofit.getAllFeedbacks()
    override suspend fun getById(id: Long): RFeedback? = retrofit.getFeedback(id).body()
}