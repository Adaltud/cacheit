package de.cacheit.storage.remote.repositories

import dagger.Provides
import de.cacheit.storage.remote.model.RPerson
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RPersonRepository @Inject constructor(): RBaseRepository<RPerson>() {
    override suspend fun createPOST(requestBody: RequestBody): Response<ResponseBody> = retrofit.createPerson(requestBody)
    override suspend fun getAllWithResponse(): Response<List<RPerson>> = retrofit.getAllPersons()
    override suspend fun getById(id: Long): RPerson? = retrofit.getPerson(id).body()
}