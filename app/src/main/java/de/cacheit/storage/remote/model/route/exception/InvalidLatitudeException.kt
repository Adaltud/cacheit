package de.cacheit.storage.remote.model.route.exception

/**
 * Should be thrown if a latitude value is not in the bounds of -90 and 90
 */
class InvalidLatitudeException(message: String?) : Exception(message)
