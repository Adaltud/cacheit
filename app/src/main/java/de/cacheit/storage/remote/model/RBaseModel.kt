package de.cacheit.storage.remote.model


/**
 * RModels have the same fiels as normal models, so the documentation can be found in the original models under:
 * src --> main --> java --> de.cachit --> model
 *
 */
abstract class RBaseModel<T>(

    var id: Long = 0, // 0 equals as not set => https://developer.android.com/reference/android/arch/persistence/room/PrimaryKey

    var created: Long = System.currentTimeMillis(),

    var modified: Long = System.currentTimeMillis()
){


    /**
     * Convert an object to a JSON string
     *
     * @return the object serialized as JSON
     */
    abstract fun toJSON() : String

    /**
     * Convert a JSON string to an instance.
     *
     * @param json The JSON string to deserialize.
     * @return The deserialized instance.
     */
    abstract fun fromJSON(json : String) : T?
}