package de.cacheit.storage.remote.repositories.route

import de.cacheit.storage.remote.model.RFeedback
import de.cacheit.storage.remote.model.route.RRoute
import de.cacheit.storage.remote.repositories.RBaseRepository
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RRouteRepository @Inject constructor() : RBaseRepository<RRoute>() {
    override suspend fun createPOST(requestBody: RequestBody): Response<ResponseBody> = retrofit.createRoute(requestBody)
    override suspend fun getAllWithResponse(): Response<List<RRoute>> = retrofit.getAllRoutes()
    override suspend fun getById(id: Long): RRoute? = retrofit.getRoute(id).body()
}