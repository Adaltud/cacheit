package de.cacheit.storage.remote.repositories.challenge

import de.cacheit.storage.remote.model.RFeedback
import de.cacheit.storage.remote.model.challenge.RTextRChallenge
import de.cacheit.storage.remote.repositories.RBaseRepository
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RTextChallengeRepository @Inject constructor() : RBaseRepository<RTextRChallenge>() {
    override suspend fun createPOST(requestBody: RequestBody): Response<ResponseBody> = retrofit.createTextChallenge(requestBody)
    override suspend fun getAllWithResponse(): Response<List<RTextRChallenge>> = retrofit.getAllTextChallenge()
    override suspend fun getById(id: Long): RTextRChallenge? = retrofit.getAllTextChallenge(id).body()
}