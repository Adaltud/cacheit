package de.cacheit.storage.remote.repositories.route

import de.cacheit.storage.remote.model.RFeedback
import de.cacheit.storage.remote.model.RPerson
import de.cacheit.storage.remote.model.route.RCache
import de.cacheit.storage.remote.repositories.RBaseRepository
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RCacheRepository @Inject constructor() : RBaseRepository<RCache>() {
    override suspend fun createPOST(requestBody: RequestBody): Response<ResponseBody> = retrofit.createCache(requestBody)
    override suspend fun getAllWithResponse(): Response<List<RCache>> = retrofit.getAllCaches()
    override suspend fun getById(id: Long): RCache? = retrofit.getCache(id).body()
}