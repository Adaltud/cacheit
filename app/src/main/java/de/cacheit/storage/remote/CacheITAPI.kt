package de.cacheit.storage.remote

import de.cacheit.storage.remote.model.RFeedback
import de.cacheit.storage.remote.model.RPerson
import de.cacheit.storage.remote.model.challenge.RMultipleChoiceRChallenge
import de.cacheit.storage.remote.model.challenge.RTextRChallenge
import de.cacheit.storage.remote.model.route.RCache
import de.cacheit.storage.remote.model.route.RCoordinates
import de.cacheit.storage.remote.model.route.RRoute
import de.cacheit.storage.remote.model.route.RRoutePart
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

/**
 *The interface get and post all model
 */
interface CacheITAPI {

    @GET ("persons")
    suspend fun getAllPersons(): Response<List<RPerson>>

    @GET ("persons/{personId}")
    suspend fun getPerson(@Path("personId") personId: Long): Response<RPerson>

    @GET ("cache")
    suspend fun getAllCaches(): Response<List<RCache>>

    @GET ("cache/{cacheId}")
    suspend fun getCache(@Path("cacheId") cacheId: Long): Response<RCache>

    @GET ("route")
    suspend fun getAllRoutes(): Response<List<RRoute>>

    @GET ("route/{routeId}")
    suspend fun getRoute(@Path("routeId") routeId: Long): Response<RRoute>

    @GET ("routepart")
    suspend fun getAllRouteParts(): Response<List<RRoutePart>>

    @GET ("routepart/{routePartId}")
    suspend fun getRoutePart(@Path("routePartId") routePartId: Long): Response<RRoutePart>

    @GET ("feedback")
    suspend fun getAllFeedbacks(): Response<List<RFeedback>>

    @GET ("feedback/{feedbackId}")
    suspend fun getFeedback(@Path("feedbackId") feedbackId: Long): Response<RFeedback>

    @GET ("coordinates")
    suspend fun getAllCoordinates(): Response<List<RCoordinates>>

    @GET ("coordinates/{coordinatesId}")
    suspend fun getCoordinates(@Path("coordinatesId") coordinatesId: Long): Response<RCoordinates>

    @GET ("multiplechoicechallenge")
    suspend fun getAllMultipleChoiceChallenge(): Response<List<RMultipleChoiceRChallenge>>

    @GET ("multiplechoicechallenge/{multiplechoicechallengeId}")
    suspend fun getMultipleChoiceChallenge(@Path("multiplechoicechallengeId") multiplechoicechallengeId: Long): Response<RMultipleChoiceRChallenge>

    @GET ("textchallenge")
    suspend fun getAllTextChallenge(): Response<List<RTextRChallenge>>

    @GET ("textchallenge/{textchallengeId}")
    suspend fun getAllTextChallenge(@Path("textchallengeId") textchallengeId: Long): Response<RTextRChallenge>

    @POST("persons")
    suspend fun createPerson(@Body requestBody: RequestBody):Response<ResponseBody>

    @POST ("cache")
    suspend fun createCache(@Body requestBody: RequestBody):Response<ResponseBody>

    @POST ("challenge")
    suspend fun createChallenge (@Body requestBody: RequestBody):Response<ResponseBody>

    @POST ("route")
    suspend fun createRoute (@Body requestBody: RequestBody):Response<ResponseBody>

    @POST ("routepart")
    suspend fun createRouteParts (@Body requestBody: RequestBody):Response<ResponseBody>

    @POST ("feedback")
    suspend fun createFeedback (@Body requestBody: RequestBody):Response<ResponseBody>

    @POST ("coordinates")
    suspend fun createCoordinates(@Body requestBody: RequestBody):Response<ResponseBody>

    @POST ("multiplechoicechallenge")
    suspend fun createMultipleChoiceChallenge(@Body requestBody: RequestBody):Response<ResponseBody>

    @POST ("textchallenge")
    suspend fun createTextChallenge(@Body requestBody: RequestBody):Response<ResponseBody>
}