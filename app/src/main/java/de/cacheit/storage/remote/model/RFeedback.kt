package de.cacheit.storage.remote.model

import android.os.Parcelable
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import de.cacheit.storage.remote.model.route.RRoute
import kotlinx.android.parcel.Parcelize


/**
 * RModels have the same fiels as normal models, so the documentation can be found in the original models under:
 * src --> main --> java --> de.cachit --> model
 *
 */
@Parcelize
data class RFeedback(


    var userId: Long = 0,

    var routeId: Long = 0,

    var feedback: String = "",

    var rating: Int = 0,

    var user: RPerson? = null,

    var route: RRoute? = null
) : RBaseModel<RFeedback>(), Parcelable {

    /**
     * Convert an object to a JSON string
     *
     * @return the object serialized as JSON
     */
    override fun toJSON(): String {
        return Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(RFeedback::class.java).toJson(
            this)
    }

    /**
     * Convert a JSON string to an instance.
     *
     * @param json The JSON string to deserialize.
     * @return The deserialized instance.
     */
    override fun fromJSON(json: String): RFeedback? {
        return Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(RFeedback::class.java).lenient().fromJson(json)
    }
}
