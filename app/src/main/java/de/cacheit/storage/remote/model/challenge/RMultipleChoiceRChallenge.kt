package de.cacheit.storage.remote.model.challenge

import android.os.Parcelable
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.android.parcel.Parcelize

/**
 * RModels have the same fiels as normal models, so the documentation can be found in the original models under:
 * src --> main --> java --> de.cachit --> model
 *
 */
@Parcelize
data class RMultipleChoiceRChallenge (

    var answers: List<String>

) : RChallenge<RMultipleChoiceRChallenge>(), Parcelable {

    /**
     * Convert an object to a JSON string
     *
     * @return the object serialized as JSON
     */
    override fun toJSON(): String {
        return Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(RMultipleChoiceRChallenge::class.java).toJson(
            this)
    }

    /**
     * Convert a JSON string to an instance.
     *
     * @param json The JSON string to deserialize.
     * @return The deserialized instance.
     */
    override fun fromJSON(json: String): RMultipleChoiceRChallenge? {
        return Moshi.Builder().add(KotlinJsonAdapterFactory()).build().adapter(RMultipleChoiceRChallenge::class.java).lenient().fromJson(json)
    }
}
