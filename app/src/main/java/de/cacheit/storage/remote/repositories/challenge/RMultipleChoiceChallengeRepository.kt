package de.cacheit.storage.remote.repositories.challenge

import de.cacheit.storage.remote.model.RFeedback
import de.cacheit.storage.remote.model.challenge.RMultipleChoiceRChallenge
import de.cacheit.storage.remote.repositories.RBaseRepository
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RMultipleChoiceChallengeRepository @Inject constructor() : RBaseRepository<RMultipleChoiceRChallenge>() {
    override suspend fun createPOST(requestBody: RequestBody): Response<ResponseBody> = retrofit.createMultipleChoiceChallenge(requestBody)
    override suspend fun getAllWithResponse(): Response<List<RMultipleChoiceRChallenge>> = retrofit.getAllMultipleChoiceChallenge()
    override suspend fun getById(id: Long): RMultipleChoiceRChallenge? = retrofit.getMultipleChoiceChallenge(id).body()
}