package de.cacheit.storage.local.dao.route

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.room.*
import de.cacheit.model.Feedback
import de.cacheit.model.Person
import de.cacheit.model.route.Cache
import de.cacheit.model.route.Coordinates
import de.cacheit.storage.local.CacheITDatabase
import de.cacheit.storage.local.dao.BaseDao
import java.util.stream.Collectors


/*
    Data Access Object definition for coordinates.
 */
@Dao
abstract class CoordinatesDao(val db: CacheITDatabase) : BaseDao<Coordinates>(db, Coordinates.TABLE_NAME, Coordinates::class.java) {

    @Transaction
    @Query("SELECT * from " + Coordinates.TABLE_NAME)
    abstract override fun getAllLive(): LiveData<List<Coordinates>>

    @Query("SELECT * from ${Coordinates.TABLE_NAME} where id = :id")
    abstract override fun getByIdLive(id: Long) : LiveData<Coordinates>


    @Transaction
    @Query("SELECT * from " + Coordinates.TABLE_NAME)
    abstract override fun getAll(): List<Coordinates>

    @Query("SELECT * from ${Coordinates.TABLE_NAME} where id = :id")
    abstract override fun getById(id: Long) : Coordinates?

    @Query("SELECT * from ${Coordinates.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getLiveDataListByIds(ids: Array<Long>) : LiveData<List<Coordinates>>

    @Query("SELECT * from ${Coordinates.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getByIds(ids: Array<Long>) : List<Coordinates>

    @Query("SELECT EXISTS(SELECT * FROM ${Coordinates.TABLE_NAME} WHERE id = :id)")
    abstract override fun exists(id: Long): Boolean
}
