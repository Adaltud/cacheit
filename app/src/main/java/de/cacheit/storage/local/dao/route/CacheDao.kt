package de.cacheit.storage.local.dao.route

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.room.*
import de.cacheit.model.Feedback
import de.cacheit.model.Person
import de.cacheit.model.challenge.TextChallenge
import de.cacheit.model.route.Cache
import de.cacheit.storage.local.CacheITDatabase
import de.cacheit.storage.local.dao.BaseDao
import java.util.stream.Collectors


/*
    Data Access Object definition for cache.
 */
@Dao
abstract class CacheDao(val db: CacheITDatabase) : BaseDao<Cache>(db, Cache.TABLE_NAME, Cache::class.java) {

    @Transaction
    @Query("SELECT * from " + Cache.TABLE_NAME)
    abstract override fun getAllLive(): LiveData<List<Cache>>

    @Query("SELECT * from ${Cache.TABLE_NAME} where id = :id")
    abstract override fun getByIdLive(id: Long) : LiveData<Cache>


    @Transaction
    @Query("SELECT * from " + Cache.TABLE_NAME)
    abstract override fun getAll(): List<Cache>

    @Query("SELECT * from ${Cache.TABLE_NAME} where id = :id")
    abstract override fun getById(id: Long) : Cache?

    @Query("SELECT * from ${Cache.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getLiveDataListByIds(ids: Array<Long>) : LiveData<List<Cache>>

    @Query("SELECT * from ${Cache.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getByIds(ids: Array<Long>) : List<Cache>

    @Query("SELECT EXISTS(SELECT * FROM ${Cache.TABLE_NAME} WHERE id = :id)")
    abstract override fun exists(id: Long): Boolean
}
