package de.cacheit.storage.local.dao.route

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.room.*
import de.cacheit.model.Feedback
import de.cacheit.model.Person
import de.cacheit.model.route.Cache
import de.cacheit.model.route.Coordinates
import de.cacheit.model.route.Route
import de.cacheit.model.route.RoutePart
import de.cacheit.storage.local.CacheITDatabase
import de.cacheit.storage.local.dao.BaseDao
import java.util.stream.Collectors


/*
    Data Access Object definition for RoutePart.
 */
@Dao
abstract class RoutePartDao(val db: CacheITDatabase) : BaseDao<RoutePart>(db, RoutePart.TABLE_NAME, RoutePart::class.java) {

    @Transaction
    @Query("SELECT * from " + RoutePart.TABLE_NAME)
    abstract override fun getAllLive(): LiveData<List<RoutePart>>

    @Query("SELECT * from ${RoutePart.TABLE_NAME} where id = :id")
    abstract override fun getByIdLive(id: Long) : LiveData<RoutePart>


    @Transaction
    @Query("SELECT * from " + RoutePart.TABLE_NAME)
    abstract override fun getAll(): List<RoutePart>

    @Query("SELECT * from ${RoutePart.TABLE_NAME} where id = :id")
    abstract override fun getById(id: Long) : RoutePart?

    @Query("SELECT * from ${RoutePart.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getLiveDataListByIds(ids: Array<Long>) : LiveData<List<RoutePart>>

    @Query("SELECT * from ${RoutePart.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getByIds(ids: Array<Long>) : List<RoutePart>

    @Query("SELECT EXISTS(SELECT * FROM ${RoutePart.TABLE_NAME} WHERE id = :id)")
    abstract override fun exists(id: Long): Boolean
}
