package de.cacheit.storage.local.dao.route

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.room.*
import de.cacheit.model.Feedback
import de.cacheit.model.Person
import de.cacheit.model.relations.RelBaseModel
import de.cacheit.model.relations.route.RelCacheCoordinates
import de.cacheit.model.relations.route.RelRoutePartCacheUnlock
import de.cacheit.model.relations.route.RelRouteStartRoutePart
import de.cacheit.model.route.Cache
import de.cacheit.model.route.Coordinates
import de.cacheit.model.route.Route
import de.cacheit.model.route.RoutePart
import de.cacheit.storage.local.CacheITDatabase
import de.cacheit.storage.local.dao.BaseDao
import java.util.stream.Collectors


/*
    Data Access Object definition for route.
 */
@Dao
abstract class RouteDao(val db: CacheITDatabase) : BaseDao<Route>(db, Route.TABLE_NAME, Route::class.java) {

    @Transaction
    @Query("SELECT * from " + Route.TABLE_NAME)
    abstract override fun getAllLive(): LiveData<List<Route>>

    @Query("SELECT * from ${Route.TABLE_NAME} where id = :id")
    abstract override fun getByIdLive(id: Long) : LiveData<Route>

    @Query("SELECT * from ${Route.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getLiveDataListByIds(ids: Array<Long>) : LiveData<List<Route>>

    @Query("SELECT * from ${Route.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getByIds(ids: Array<Long>) : List<Route>

    @Transaction
    @Query("SELECT * from " + Route.TABLE_NAME)
    abstract override fun getAll(): List<Route>

    @Query("SELECT EXISTS(SELECT * FROM ${Route.TABLE_NAME} WHERE id = :id)")
    abstract override fun exists(id: Long): Boolean

    @Query("SELECT * from ${Route.TABLE_NAME} where id = :id")
    abstract override fun getById(id: Long) : Route?

    @Query("SELECT A.* from ${Route.TABLE_NAME} as A " +
            "inner join ${RelRouteStartRoutePart.TABLE_NAME} as B ON B.${RelBaseModel.PARENTFIELDNAME} = A.id " +
            "inner join ${RoutePart.TABLE_NAME} as C ON B.${RelBaseModel.CHILDFIELDNAME} = C.id " +
            "inner join ${RelRoutePartCacheUnlock.TABLE_NAME} as D ON D.${RelBaseModel.PARENTFIELDNAME} = C.id " +
            "inner join ${Cache.TABLE_NAME} as E ON D.${RelBaseModel.CHILDFIELDNAME} = E.id " +
            "inner join ${RelCacheCoordinates.TABLE_NAME} as F ON F.${RelBaseModel.PARENTFIELDNAME} = E.id " +
            "inner join ${Coordinates.TABLE_NAME} as G ON F.${RelBaseModel.CHILDFIELDNAME} = G.id " +
            "where G.latitude BETWEEN :latitudeLowerBound AND :latitudeUpperBound " +
            "and G.longitude BETWEEN :longitudeLowerBound AND :longitudeUpperBound " +
            "limit :limit")
    abstract suspend fun getRoutesInBoundAwait(latitudeLowerBound: Double, latitudeUpperBound: Double,
                                  longitudeLowerBound: Double, longitudeUpperBound: Double, limit: Int) : List<Route>

    @Query("SELECT C.id from ${Route.TABLE_NAME} as A " +
            "inner join ${RelRouteStartRoutePart.TABLE_NAME} as B ON B.${RelBaseModel.PARENTFIELDNAME} = A.id " +
            "inner join ${RoutePart.TABLE_NAME} as C ON B.${RelBaseModel.CHILDFIELDNAME} = C.id " +
            "where A.id = :id")
    abstract suspend fun getStartRoutePartIdAwait(id: Long) : Long
}
