package de.cacheit.storage.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import de.cacheit.model.Feedback
import de.cacheit.model.Person
import de.cacheit.model.relations.RelPersonFriends
import de.cacheit.storage.local.CacheITDatabase
import kotlin.reflect.KClass


/*
    Data Access Object definition for Persons.
 */
@Dao
abstract class PersonDao(val db: CacheITDatabase) : BaseDao<Person>(db, Person.TABLE_NAME, Person::class.java) {
    @Transaction
    @Query("SELECT * from " + Person.TABLE_NAME)
    abstract override fun getAllLive(): LiveData<List<Person>>

    @Query("SELECT * from ${Person.TABLE_NAME} where id = :id")
    abstract override fun getByIdLive(id: Long) : LiveData<Person>

    @Transaction
    @Query("SELECT * from " + Person.TABLE_NAME)
    abstract override fun getAll(): List<Person>

    @Query("SELECT * from ${Person.TABLE_NAME} where id = :id")
    abstract override fun getById(id: Long) : Person?

    @Query("SELECT * from ${Person.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getLiveDataListByIds(ids: Array<Long>) : LiveData<List<Person>>

    @Query("SELECT * from ${Person.TABLE_NAME} as A where A.email = :email and A.password = :password")
    abstract suspend fun checkLogin(email: String,password: String) : Person?

    @Query("SELECT * from ${Person.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getByIds(ids: Array<Long>) : List<Person>

    @Query("SELECT EXISTS(SELECT * FROM ${Person.TABLE_NAME} WHERE id = :id)")
    abstract override fun exists(id: Long): Boolean
}
