package de.cacheit.storage.local.dao.challenge

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.room.*
import de.cacheit.model.Feedback
import de.cacheit.model.Person
import de.cacheit.model.challenge.MultipleChoiceChallenge
import de.cacheit.model.challenge.TextChallenge
import de.cacheit.model.route.Cache
import de.cacheit.model.route.Route
import de.cacheit.storage.local.CacheITDatabase
import de.cacheit.storage.local.dao.BaseDao
import java.util.stream.Collectors


/*
    Data Access Object definition for MultipleChoiceChallenge.
 */
@Dao
abstract class MultipleChoiceChallengeDao(val db: CacheITDatabase)
    : BaseDao<MultipleChoiceChallenge>(db, MultipleChoiceChallenge.TABLE_NAME, MultipleChoiceChallenge::class.java) {

    @Transaction
    @Query("SELECT * from " + MultipleChoiceChallenge.TABLE_NAME)
    abstract override fun getAllLive(): LiveData<List<MultipleChoiceChallenge>>

    @Query("SELECT * from ${MultipleChoiceChallenge.TABLE_NAME} where id = :id")
    abstract override fun getByIdLive(id: Long) : LiveData<MultipleChoiceChallenge>

    @Transaction
    @Query("SELECT * from " + MultipleChoiceChallenge.TABLE_NAME)
    abstract override fun getAll(): List<MultipleChoiceChallenge>

    @Query("SELECT * from ${MultipleChoiceChallenge.TABLE_NAME} where id = :id")
    abstract override fun getById(id: Long) : MultipleChoiceChallenge?

    @Query("SELECT * from ${MultipleChoiceChallenge.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getLiveDataListByIds(ids: Array<Long>) : LiveData<List<MultipleChoiceChallenge>>

    @Query("SELECT * from ${MultipleChoiceChallenge.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getByIds(ids: Array<Long>) : List<MultipleChoiceChallenge>

    @Query("SELECT EXISTS(SELECT * FROM ${MultipleChoiceChallenge.TABLE_NAME} WHERE id = :id)")
    abstract override fun exists(id: Long): Boolean
}
