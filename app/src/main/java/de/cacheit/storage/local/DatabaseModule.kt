package de.cacheit.storage.local

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import de.cacheit.storage.local.dao.FeedbackDao
import de.cacheit.storage.local.dao.PersonDao
import de.cacheit.storage.local.dao.challenge.MultipleChoiceChallengeDao
import de.cacheit.storage.local.dao.challenge.TextChallengeDao
import de.cacheit.storage.local.dao.route.CacheDao
import de.cacheit.storage.local.dao.route.CoordinatesDao
import de.cacheit.storage.local.dao.route.RouteDao
import de.cacheit.storage.local.dao.route.RoutePartDao
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
class DatabaseModule {

    // region provide DAOs
    @Provides
    fun providePersonDao(appDatabase: CacheITDatabase): PersonDao {
        return appDatabase.personDao()
    }

    @Provides
    fun provideFeedbackDao(appDatabase: CacheITDatabase): FeedbackDao {
        return appDatabase.feedbackDao()
    }

    @Provides
    fun provideCacheDao(appDatabase: CacheITDatabase): CacheDao {
        return appDatabase.cacheDao()
    }

    @Provides
    fun provideCoordinatesDao(appDatabase: CacheITDatabase): CoordinatesDao {
        return appDatabase.coordinatesDao()
    }

    @Provides
    fun provideRouteDao(appDatabase: CacheITDatabase): RouteDao {
        return appDatabase.routeDao()
    }

    @Provides
    fun provideRoutePartDao(appDatabase: CacheITDatabase): RoutePartDao {
        return appDatabase.routePartDao()
    }

    @Provides
    fun provideTextChallengeDao(appDatabase: CacheITDatabase): TextChallengeDao {
        return appDatabase.textChallengeDao()
    }

    @Provides
    fun provideMultipleChoiceChallengeDao(appDatabase: CacheITDatabase): MultipleChoiceChallengeDao {
        return appDatabase.multipleChoiceChallengeDao()
    }
    // endregion

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): CacheITDatabase {
        return Room.databaseBuilder(
            appContext,
            CacheITDatabase::class.java,
            "cacheit_db"
        ).build()
    }
}