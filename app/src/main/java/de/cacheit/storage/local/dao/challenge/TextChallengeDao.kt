package de.cacheit.storage.local.dao.challenge

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.room.*
import de.cacheit.model.Feedback
import de.cacheit.model.Person
import de.cacheit.model.challenge.MultipleChoiceChallenge
import de.cacheit.model.challenge.TextChallenge
import de.cacheit.model.route.Cache
import de.cacheit.storage.local.CacheITDatabase
import de.cacheit.storage.local.dao.BaseDao
import java.util.stream.Collectors


/*
    Data Access Object definition for TextChallenge.
 */
@Dao
abstract class TextChallengeDao(val db: CacheITDatabase) : BaseDao<TextChallenge>(db, TextChallenge.TABLE_NAME, TextChallenge::class.java) {

    @Transaction
    @Query("SELECT * from " + TextChallenge.TABLE_NAME)
    abstract override fun getAllLive(): LiveData<List<TextChallenge>>

    @Query("SELECT * from ${TextChallenge.TABLE_NAME} where id = :id")
    abstract override fun getByIdLive(id: Long) : LiveData<TextChallenge>


    @Transaction
    @Query("SELECT * from " + TextChallenge.TABLE_NAME)
    abstract override fun getAll(): List<TextChallenge>

    @Query("SELECT * from ${TextChallenge.TABLE_NAME} where id = :id")
    abstract override fun getById(id: Long) : TextChallenge?

    @Query("SELECT * from ${TextChallenge.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getLiveDataListByIds(ids: Array<Long>) : LiveData<List<TextChallenge>>

    @Query("SELECT * from ${TextChallenge.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getByIds(ids: Array<Long>) : List<TextChallenge>

    @Query("SELECT EXISTS(SELECT * FROM ${TextChallenge.TABLE_NAME} WHERE id = :id)")
    abstract override fun exists(id: Long): Boolean
}
