package de.cacheit.storage.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import de.cacheit.model.Feedback
import de.cacheit.model.Person
import de.cacheit.model.route.RoutePart
import de.cacheit.storage.local.CacheITDatabase


/*
    Data Access Object definition for feedback.
 */
@Dao
abstract class FeedbackDao(val db: CacheITDatabase) : BaseDao<Feedback>(db, Feedback.TABLE_NAME, Feedback::class.java) {

    @Transaction
    @Query("SELECT * from " + Feedback.TABLE_NAME)
    abstract override fun getAllLive(): LiveData<List<Feedback>>

    @Query("SELECT * from ${Feedback.TABLE_NAME} where id = :id")
    abstract override fun getByIdLive(id: Long) : LiveData<Feedback>


    @Transaction
    @Query("SELECT * from " + Feedback.TABLE_NAME)
    abstract override fun getAll(): List<Feedback>

    @Query("SELECT * from ${Feedback.TABLE_NAME} where id = :id")
    abstract override fun getById(id: Long) : Feedback?

    @Query("SELECT * from ${Feedback.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getLiveDataListByIds(ids: Array<Long>) : LiveData<List<Feedback>>

    @Query("SELECT * from ${Feedback.TABLE_NAME} as A where A.id in (:ids)")
    abstract override fun getByIds(ids: Array<Long>) : List<Feedback>

    @Query("SELECT EXISTS(SELECT * FROM ${Feedback.TABLE_NAME} WHERE id = :id)")
    abstract override fun exists(id: Long): Boolean
}
