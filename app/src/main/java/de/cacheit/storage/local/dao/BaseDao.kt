package de.cacheit.storage.local.dao

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.sqlite.db.SimpleSQLiteQuery
import androidx.sqlite.db.SupportSQLiteQuery
import de.cacheit.core.extensions.readInstanceProperty
import de.cacheit.model.BaseModel
import de.cacheit.model.relations.RelBaseModel
import de.cacheit.model.relations.RelPersonFriends
import de.cacheit.model.relations.Relation
import de.cacheit.model.route.Cache
import de.cacheit.storage.IStorageResolver
import de.cacheit.storage.local.CacheITDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.lang.Exception
import java.lang.reflect.Field
import java.util.*
import java.util.concurrent.Callable
import javax.inject.Singleton
import kotlin.collections.ArrayList
import kotlin.reflect.*
import kotlin.reflect.full.functions
import kotlin.reflect.jvm.kotlinProperty

abstract class BaseDao<T>(@SuppressWarnings("WeakerAccess")
                          protected val dB: CacheITDatabase,
                          private val tableName: String,
                          val modelClazz: Class<T> )
        where T : BaseModel {


    private var storageResolver: MutableList<IStorageResolver<T>> = mutableListOf()

    fun addResolver(resolver: IStorageResolver<T>) = storageResolver.add(resolver)
    fun removeResolver(resolver: IStorageResolver<T>) = storageResolver.remove(resolver)


    @SuppressWarnings("WeakerAccess")
    protected var daoFetcher = DaoFetcher(dB)

    /**
     * TODO
     *
     * @param id
     * @return
     */
    abstract fun exists(id: Long) : Boolean

    @Insert
    abstract fun insert(entity: T): Long
    @Insert
    abstract fun insert(addresses: List<T>): List<Long>
    @Insert
    abstract fun insert(vararg obj: T): List<Long>
    @Insert
    abstract suspend fun insertAwait(entity: T): Long
    @Insert
    abstract suspend fun insertAwait(addresses: List<T>): List<Long>
    @Insert
    abstract suspend fun insertAwait(vararg obj: T): List<Long>


    @Transaction
    open fun insertComplexAuto(entity: T): Long = runBlocking { insertComplexAutoInternal(listOf(entity)).first() }
    @Transaction
    open fun insertComplexAuto(entities: List<T>): List<Long> = runBlocking { insertComplexAutoInternal(entities) }
    @Transaction
    open fun insertComplexAuto(vararg entities: T): List<Long> = runBlocking { insertComplexAutoInternal(entities.asList()) }
    @Transaction
    open suspend fun insertComplexAutoAwait(entity: T): Long = insertComplexAutoInternal(listOf(entity)).first()
    @Transaction
    open suspend fun insertComplexAutoAwait(entities: List<T>): List<Long> = insertComplexAutoInternal(entities)
    @Transaction
    open suspend fun insertComplexAutoAwait(vararg entities: T): List<Long> = insertComplexAutoInternal(entities.asList())


    @Update
    abstract fun update(entity: T)
    @Update
    abstract fun update(entities: List<T>)
    @Update
    abstract fun update(vararg entities: T)
    @Update
    abstract suspend fun updateAwait(entity: T)
    @Update
    abstract suspend fun updateAwait(entities: List<T>)
    @Update
    abstract suspend fun updateAwait(vararg entities: T)

    @Transaction
    open fun updateComplexAuto(entity: T) = runBlocking { updateComplexAutoInternal(listOf(entity)) }
    @Transaction
    open fun updateComplexAuto(entities: List<T>) = runBlocking { updateComplexAutoInternal(entities) }
    @Transaction
    open fun updateComplexAuto(vararg entities: T) = runBlocking { updateComplexAutoInternal(entities.asList()) }
    @Transaction
    open suspend fun updateComplexAutoAwait(entity: T) = updateComplexAutoInternal(listOf(entity))
    @Transaction
    open suspend fun updateComplexAutoAwait(entities: List<T>) = updateComplexAutoInternal(entities)
    @Transaction
    open suspend fun updateComplexAutoAwait(vararg entities: T) = updateComplexAutoInternal(entities.asList())

    @Delete
    abstract fun delete(entity: T) : Int
    @Delete
    abstract fun delete(entities: List<T>) : Int
    @Delete
    abstract fun delete(vararg entities: T) : Int
    @Delete
    abstract suspend fun deleteAwait(entity: T) : Int
    @Delete
    abstract suspend fun deleteAwait(entities: List<T>) : Int
    @Delete
    abstract suspend fun deleteAwait(vararg entities: T) : Int

    // Delete all
    @RawQuery
    protected abstract suspend fun deleteAll(query: SupportSQLiteQuery): Int
    fun deleteAll() = runBlocking {  deleteAll(SimpleSQLiteQuery("DELETE FROM $tableName")) }
    suspend fun deleteAllAwait() = deleteAll(SimpleSQLiteQuery("DELETE FROM $tableName"))

    // Get
    abstract fun getById(id: Long) : T?
    abstract fun getAll(): List<T>
    suspend fun getByIdAwait(id: Long) =  withContext(Dispatchers.Default) { getById(id) }
    suspend fun getAllAwait() =  withContext(Dispatchers.Default) { getAll() }

    // Do NOT use suspend when returning LiveData!
    abstract fun getByIdLive(id: Long) : LiveData<T>
    abstract fun getAllLive(): LiveData<List<T>>
    abstract fun getLiveDataListByIds(ids: Array<Long>) : LiveData<List<T>>
    abstract fun getByIds(ids: Array<Long>) : List<T>

    fun getByIdComplexAuto(id: Long, vararg fieldNames: String ) = runBlocking {  getByIdComplexAutoImp(id, fieldNames.asList()) }
    fun getByIdComplexAuto(id: Long, fieldNames: List<String> ) = runBlocking { getByIdComplexAutoImp(id, fieldNames) }
    fun getByIdComplexAuto(id: Long) = runBlocking { getByIdComplexAutoImp(id, listOf())}

    suspend fun getByIdComplexAutoAwait(id: Long, fieldNames: List<String> ) = getByIdComplexAutoImp(id, fieldNames)
    suspend fun getByIdComplexAutoAwait(id: Long) = getByIdComplexAutoImp(id, listOf())

    private suspend fun getByIdComplexAutoImp(id: Long, fieldNames: List<String>) : T?
    {
        // Retrieve the parent entity
        val entity = getByIdAwait(id)

        // Should entity be set, retrieve the children
        if (entity != null) {
            for (field in modelClazz.declaredFields) {
                if (field.isAnnotationPresent(Relation::class.java) && field.kotlinProperty is KMutableProperty1<*,*>
                    && (fieldNames.isNullOrEmpty() || fieldNames.contains(field.name))) {

                    // Retrieve the annotation properties
                    val (tableName, _, childDaoClass) = retrieveRelationAnnotation(field)
                    checkRelationAnnotation(tableName, childDaoClass, true)

                    // retrieve the already stored joins
                    val currentJoins = fetchJoins(entity.id, tableName, false)
                    val values: MutableList<Any> = mutableListOf()

                    if (currentJoins.isEmpty()) {
                        if (field.type == List::class.java)
                            (field.kotlinProperty as KMutableProperty1<*, *>).setter.call(entity, values)
                    }
                    else {

                        // Get an instance of the child dao
                        val childDao = daoFetcher.fetch(childDaoClass)

                        if (field.type == List::class.java) {

                            // Target field is a collection, add all joins
                            currentJoins.forEach {
                                if (it.parentId == id) {

                                    // Retrieve the child entity and add it to the result set
                                    var child =  childDao.getByIdAwait(it.childId)
                                    if (child == null){
                                        for (resolver in storageResolver) {
                                            child = resolver.getById(it.childId)
                                            if (child != null)
                                                break
                                        }
                                    }
                                    if (child != null) {
                                        values.add(child)
                                    }
                                }
                            }
                            (field.kotlinProperty as KMutableProperty1<*, *>).setter.call(entity, values)
                        }
                        else if (BaseModel::class.java.isAssignableFrom(field.type)) {

                            // Target field is a single field, add first join
                            val child = childDao.getByIdAwait(currentJoins.first().childId)
                            if (child != null)
                                (field.kotlinProperty as KMutableProperty1<*, *>).setter.call(entity, child)
                        }
                    }
                }
            }
        }
        return entity
    }

    /**
     * Retrieves following annotation properties:
     * - relation table name [String]
     * - bidirectional [Boolean]
     * - Dao class for child [KClass<*>]
     *
     * @param field The field with the annotation.
     * @return Triple with the retrieved properties.
     */
    private fun retrieveRelationAnnotation(field: Field): Triple<String, Boolean, KClass<*>> {
        val tableName = field.getAnnotation(Relation::class.java)!!.tableName
        val bidirectional = field.getAnnotation(Relation::class.java)!!.bidirectional
        val childDaoClass = field.getAnnotation(Relation::class.java)!!.childDao
        return Triple(tableName, bidirectional, childDaoClass)
    }

    /**
     * Checks if the annotation properties are correctly set.
     *
     * @param childTableName The table name of relation table
     * @param daoClass The dao class for the child entity
     * @param shouldThrow Determines if an exception should be thrown if a property is not correctly set.
     * @return True if all properties are correctly set, otherwise false.
     */
    private fun checkRelationAnnotation(childTableName: String, daoClass: KClass<*>, shouldThrow: Boolean) : Boolean {
        var ret = false

        if (childTableName.isNotEmpty() && daoClass !is BaseDao<*>)
            ret = true
        else if (shouldThrow) // Todo exception, should not be necessary with the annotation processor
            throw Exception("Exception to be added")

        return ret
    }

    private suspend fun insertComplexAutoInternal(entities: List<T>) : List<Long> {
        val ids = ArrayList<Long>()
        entities.forEach {
            it.id = insertAwait(it)
            ids.add(it.id)
            updateJoinsAuto(it)
        }
        return ids
    }

    private suspend fun updateComplexAutoInternal(entities: List<T>) {
        entities.forEach {
            update(it)
            updateJoinsAuto(it)
        }
    }

    private suspend fun updateJoinsAuto(entity: T) {
        // Loop over each field of class and check for Relation annotation
        for (field in modelClazz.declaredFields) {
            if (field.isAnnotationPresent(Relation::class.java)) {

                // Found a relation field, load annotation infos
                val (tableName, bidirectional, childDaoClass) = retrieveRelationAnnotation(field)
                checkRelationAnnotation(tableName, childDaoClass, true)

                // load current joins for this field
                val currentJoins = fetchJoins(entity.id, tableName, bidirectional)
                var values: MutableList<BaseModel>? = null

                if (field.type == List::class.java)
                    values = readInstanceProperty<MutableList<BaseModel>>(entity, field.name)
                else if (BaseModel::class.java.isAssignableFrom(field.type)) {
                    val value = readInstanceProperty<BaseModel>(entity, field.name)
                    if (value != null)
                        values = mutableListOf(value)
                }

                // If values is null, no children were fetched for this field
                // In that case, do not update anything for this field
                if (values == null)
                    continue

                // first go through current joins and search for disjoined ones
                var itIndex = 0
                while (itIndex < currentJoins.size)
                {
                    // If join is not in current values list, it means that the join should be removed
                    if (!values.any { x -> x.id == currentJoins[itIndex].childId }) {
                        disjoin(entity.id, currentJoins[itIndex].childId, tableName, bidirectional)
                        currentJoins.removeAt(itIndex)
                    }
                    else
                        itIndex++
                }

                // Check if nested object needs to be inserted
                val childDao = daoFetcher.fetch(childDaoClass)
                values.forEach {
                    // Okay... so this is a very very.... very ugly quick and dirty solution....
                    if (!childDao.exists(it.id)) {
                        try {
                            childDao::class.java.getMethod("insertComplexAuto", it.javaClass).invoke(childDao, it)
                        } catch (ex:Exception) {
                            Log.e("InsertComplexAuto", "Error while inserting nested object.", ex)
                        }
                    }
                }


                // Next, go through all the child elements for the current field
                values.forEach {
                    // If join is not in currentJoins, it means that it is a new join
                    if (!currentJoins.any { x -> x.childId == it.id })
                        join(entity.id, it.id, tableName, bidirectional)
                }
            }
        }
    }

    @RawQuery
    abstract suspend fun fetchJoinsInternal(query: SupportSQLiteQuery) : List<RelBaseModel>
    private suspend fun fetchJoins(parentId: Long, tableName: String, bidirectional: Boolean = false): MutableList<RelBaseModel> {
        var query = "SELECT id, ${RelBaseModel.PARENTFIELDNAME}, ${RelBaseModel.CHILDFIELDNAME} FROM $tableName WHERE ${RelBaseModel.PARENTFIELDNAME} = $parentId"

        if (bidirectional)
            query += " OR ${RelBaseModel.CHILDFIELDNAME} = $parentId"

        val simpleSQLiteQuery = SimpleSQLiteQuery(query, arrayOf())
        return fetchJoinsInternal(simpleSQLiteQuery).toMutableList()
    }

    private suspend fun joinChildren(parentId: Long, children: MutableList<BaseModel>, relTable: String)
        = children.forEach { joinChild(parentId, it.id, relTable) }


    private suspend fun joinChild(parentId: Long, childId: Long, relTable: String) {
        if (childId != 0L && !areJoined(parentId, childId, relTable))  // only join already created children
            join(parentId, childId, RelPersonFriends.TABLE_NAME)
    }

    @RawQuery
    abstract fun areJoinedInternal(query: SupportSQLiteQuery) : Long

    /* Checks whether the provided entities are joined */
    private fun areJoined(parentId: Long, childId: Long, relTable: String): Boolean {
        val query = "SELECT count(*) FROM $relTable where parentId = ? and childId = ? LIMIT 1"
        val simpleSQLiteQuery = SimpleSQLiteQuery(query, arrayOf(parentId, childId))
        return areJoinedInternal(simpleSQLiteQuery) > 0
    }

    @RawQuery
    abstract suspend fun joinInternal(query: SupportSQLiteQuery) : Long

    /* Join the provided entities */
    private suspend fun join(parentId: Long, childId: Long, relTable: String, bidirectional: Boolean = false) {
        if (parentId > 0 && childId > 0) {
            val query = "INSERT INTO $relTable (${RelBaseModel.PARENTFIELDNAME}, ${RelBaseModel.CHILDFIELDNAME}) VALUES (?, ?)"
            joinInternal(SimpleSQLiteQuery(query, arrayOf(parentId, childId)))

            if (bidirectional)
                joinInternal(SimpleSQLiteQuery(query, arrayOf(childId, parentId)))
        }
    }

    @RawQuery
    abstract suspend fun disjoinInternal(query: SupportSQLiteQuery) : Long

    /* Disjoin the provided entities */
    private suspend fun disjoin(parentId: Long, childId: Long, relTable: String, bidirectional: Boolean = false) {
        if (parentId > 0 && childId > 0) {
            val query = "DELETE FROM $relTable WHERE ${RelBaseModel.PARENTFIELDNAME} = ? AND ${RelBaseModel.CHILDFIELDNAME} = ?"
            disjoinInternal(SimpleSQLiteQuery(query, arrayOf(parentId, childId)))

            if (bidirectional)
                disjoinInternal(SimpleSQLiteQuery(query, arrayOf(childId, parentId)))
        }
    }

    @Singleton
    class DaoFetcher(var db: CacheITDatabase) {

        private var daoMap: MutableMap<Class<*>, Callable<BaseDao<*>>> = mutableMapOf()

        init {
            // Will be using java reflection, as it seems to be waaaay faster than Kotlin (benchmarked 0.01s vs > 2s)
            for (func in db::class.java.declaredMethods) {
                if (BaseDao::class.java.isAssignableFrom(func.returnType)) {
                    daoMap[func.returnType] = Callable {
                        func.invoke(db) as BaseDao<*>
                    }
                }
            }
        }

        fun fetch(daoClass: KClass<*>) : BaseDao<*>
        {
            if (!daoMap.containsKey(daoClass.java))
                throw Exception("Method for getting DAO is missing in the")

            val va = daoMap[daoClass.java]?.call()
            return va!!
        }

        fun <T : BaseDao<*>> fetchT(daoClass: KClass<*>) : T
        {
            if (!daoMap.containsKey(daoClass.java))
                throw Exception("Method for getting DAO is missing in the")

            val va = daoMap[daoClass.java]?.call()
            return va as T
        }
    }
}