package de.cacheit.storage.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import de.cacheit.model.Feedback
import de.cacheit.model.Person
import de.cacheit.model.challenge.MultipleChoiceChallenge
import de.cacheit.model.challenge.TextChallenge
import de.cacheit.model.data.ListStringTypeConverter
import de.cacheit.model.relations.*
import de.cacheit.model.relations.challenge.RelMultipleChoiceChallengeCreatedBy
import de.cacheit.model.relations.challenge.RelTextChallengeCreatedBy
import de.cacheit.model.relations.route.*
import de.cacheit.model.route.Cache
import de.cacheit.model.route.Coordinates
import de.cacheit.model.route.Route
import de.cacheit.model.route.RoutePart
import de.cacheit.storage.local.dao.FeedbackDao
import de.cacheit.storage.local.dao.PersonDao
import de.cacheit.storage.local.dao.challenge.MultipleChoiceChallengeDao
import de.cacheit.storage.local.dao.challenge.TextChallengeDao
import de.cacheit.storage.local.dao.route.CacheDao
import de.cacheit.storage.local.dao.route.CoordinatesDao
import de.cacheit.storage.local.dao.route.RouteDao
import de.cacheit.storage.local.dao.route.RoutePartDao

/*
   Our Database Management class
*/
@Database( entities = [Person::class, Feedback::class, RoutePart::class, Route::class, Cache::class,
                       TextChallenge::class, MultipleChoiceChallenge::class, Coordinates::class,
    // region RELATIONS
                        RelPersonFriends::class, RelPersonSavedRoutes::class, RelPersonSolvedRoutes::class,
                        RelFeedbackRoute::class, RelCacheCoordinates::class,
                        RelRoutePartCacheFinish::class, RelRoutePartCacheUnlock::class,
                        RelRoutePartChallenge::class, RelRouteStartRoutePart::class, RelRouteRouteParts::class,
    // endregion RELATIONS
    // region CREATED BY RELATIONS
                        RelCacheCreatedBy::class, RelRouteCreatedBy::class, RelRoutePartCreatedBy::class,
                        RelMultipleChoiceChallengeCreatedBy::class, RelTextChallengeCreatedBy::class,
                        RelFeedbackCreatedBy::class],
    // endregion CREATED BY RELATIONS
            version = 1 )
@TypeConverters(ListStringTypeConverter::class)
abstract class CacheITDatabase : RoomDatabase() {
    private val LOG_TAG_DB = "PersonDB"

    // region DAO references, will be filled by Android
    abstract fun personDao(): PersonDao
    abstract fun feedbackDao(): FeedbackDao
    abstract fun cacheDao(): CacheDao
    abstract fun coordinatesDao(): CoordinatesDao
    abstract fun routeDao(): RouteDao
    abstract fun routePartDao(): RoutePartDao
    abstract fun textChallengeDao(): TextChallengeDao
    abstract fun multipleChoiceChallengeDao(): MultipleChoiceChallengeDao
    // endregion DAO references

    // No need to use ExecutorService. We will be using kotlin coroutines.
}
