package de.cacheit.storage

import android.util.Log
import de.cacheit.model.BaseModel
import de.cacheit.storage.remote.model.RBaseModel
import org.modelmapper.ModelMapper
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Helper class to map one object to another.
 *
 */
@Singleton
class LocalRemoteMapper @Inject constructor() {

    // Since JVM generics sucks, we will need multiple map methods
    // Some are nice, which do not require clazz, and some suck and do require clazz...

    inline fun <TSource : BaseModel, reified TTarget : RBaseModel<*>> map(entity: TSource?) : TTarget? {
        if (entity == null) return null
        return ModelMapper().map(entity, TTarget::class.java)
    }

    inline fun <TSource : RBaseModel<*>, reified TTarget : BaseModel> map(entity: TSource?) : TTarget? {
        if (entity == null) return null
        return ModelMapper().map(entity, TTarget::class.java)
    }


    inline fun <TTarget : BaseModel> map(entity: Any?, targetClass: Class<*>) : TTarget? {
        if (entity == null) return null
        try {
            val result = ModelMapper().map(entity, targetClass)
            return if (result != null) result as TTarget else null
        }
        catch (ex: java.lang.Exception) {
            Log.e("LocalRemoteMapper", "Error converting object.", ex)
        }
        return null
    }

    inline fun <reified TTarget> map(entity: Any?) : TTarget? {
        if (entity == null) return null
        val result = ModelMapper().map(entity, TTarget::class.java)
        return result as TTarget
    }


    fun <T : BaseModel, K : RBaseModel<*>> map(entities: Collection<K>?, targetClass: Class<*>) : List<T> {
        val result = mutableListOf<T>()
        entities?.forEach {
                var record = map<T>(it, targetClass)
                if (record != null)
                    result.add(record)
        }
        return result
    }
}